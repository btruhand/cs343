#include <iostream>
using namespace std;

class A{
  public:
    int a;
	virtual void test() {
		cout << "Print A" << endl;
	}

	A() { a = 3; }
};

class B : public A{
  public:
	void test() {
		cout << "Print B" << endl;
	}
};

int main() {
	A* test[1];
	B* stuff = new B;
	test[0] = stuff;
	test[0]->test();
	cout << test[0]->a << endl;
	delete stuff;
}
