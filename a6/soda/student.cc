#include "student.h"
#include "printer.h"
#include "server.h"
#include "office.h"
#include "watcard.h"
#include "vm.h"
#include "MPRNG.h"

extern MPRNG randNum;

Student::Student( Printer &prt, NameServer &nameServer, WATCardOffice &cardOffice, unsigned int id, 
	unsigned int maxPurchases ) : printer(prt), nameServer(nameServer), cardOffice(cardOffice), id(id), maxPurchases(maxPurchases) {}

void Student::main() {
	unsigned int favSoda = randNum( 3 ); 
	unsigned int numPurchased = randNum( 1, maxPurchases );
	printer.print(Printer::Student, id, (char) Start, favSoda, numPurchased);
	VendingMachine *vm = nameServer.getMachine(id); // blocks possibly
	printer.print(Printer::Student, id, (char) Select_VM, vm->getId());
	WATCard::FWATCard watcard = cardOffice.create(id, 5);
	
	yield(randNum(1,10)); // yield before first buy
	for (unsigned int bought = 0;;) { // loop but don't increment or decrement
		try {
			myRealCard = watcard(); // get my card from the future
			vm->buy( (VendingMachine::Flavours)favSoda, *myRealCard );
			printer.print(Printer::Student, id, (char) Bought_Soda, myRealCard->getBalance());
			if(++bought == numPurchased) break; // increment only when buy is successful, get out of loop if all sodas are bought
		} catch (WATCardOffice::Lost) {
			watcard = cardOffice.create(id, 5); // create card with $5
			printer.print(Printer::Student, id, (char) WATCard_Lost);
			continue; // don't want to yield
		} catch (VendingMachine::Funds) {
			watcard = cardOffice.transfer(id, 5 + vm->cost(), myRealCard); // top up to card 5+sodaCost
		} catch (VendingMachine::Stock) {
			vm = nameServer.getMachine(id); // look for next machine
			printer.print(Printer::Student, id, (char) Select_VM, vm->getId());
		}
		
		yield(randNum(1,10)); // yield before next buy
	}
	
	printer.print(Printer::Student, id, (char) Finished);
}

Student::~Student() {
	delete myRealCard; // delete my card
}
