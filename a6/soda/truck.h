#ifndef __TRUCK_H__
#define __TRUCK_H__

_Cormonitor Printer;
_Task NameServer;
_Task BottlingPlant;

_Task Truck {
	Printer &printer;
	NameServer &nameServer;
	BottlingPlant &plant;
	unsigned int numVendingMachines;
	unsigned int maxStockPerFlavour;
	enum States { Start = 'S', PickedUp = 'P', BeginDelivery = 'd', Unsuccessfull = 'U',  EndDelivery = 'D', Finished = 'F' };
	void main();
  public:
	Truck( Printer &prt, NameServer &nameServer, BottlingPlant &plant,
	   unsigned int numVendingMachines, unsigned int maxStockPerFlavour );
};

#endif // __TRUCK_H__
