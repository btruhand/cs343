#ifndef __STUDENT_H__
#define __STUDENT_H__

_Cormonitor Printer;
_Task NameServer;
_Task WATCardOffice;
class WATCard;

_Task Student {
	Printer &printer;
	NameServer &nameServer;
	WATCardOffice &cardOffice;
	unsigned int id;
	unsigned int maxPurchases;
	WATCard* myRealCard; // the student's actual card
	enum States { Start = 'S', Select_VM = 'V', Bought_Soda = 'B', WATCard_Lost = 'L', Finished = 'F' };
	void main();
  public:
	Student( Printer &prt, NameServer &nameServer, WATCardOffice &cardOffice, unsigned int id,
	     unsigned int maxPurchases );
	~Student();
};

#endif // __STUDENT_H__
