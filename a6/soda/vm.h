#ifndef __VM_H__
#define __VM_H__

_Cormonitor Printer;
_Task NameServer;
class WATCard;

_Task VendingMachine {
	Printer& printer;
	NameServer& nameServer;
	unsigned int VMid;
	unsigned int sodaCost;
	unsigned int maxStock; // max stock per flavour

	unsigned int sodaStock[4]; // stock of soda
	WATCard* studentCard; // get the card when student calls buy
	uCondition studentWait; // where students wait for their soda

	void main();
  public:
	enum Flavours { BLUESCHERRY, CLASSICSODA, ROOTBEER, JAZZLIME };                 // flavours of soda (YOU DEFINE)
	enum ExpType { OUTOFSTOCK, OUTOFFUNDS, NOEXP }; // exception types for throwing _Stock or _Fund or nothing at all
	enum States { Start = 'S', Reload = 'r', CompleteReload = 'R', Bought = 'B', Finished = 'F' };
	_Event Funds {};                       // insufficient funds
	_Event Stock {};                       // out of stock for particular flavour
	VendingMachine( Printer &prt, NameServer &nameServer, unsigned int id, unsigned int sodaCost,
		    unsigned int maxStockPerFlavour );
	void buy( Flavours flavour, WATCard &card );
	unsigned int *inventory();
	void restocked();
	_Nomutex unsigned int cost();
	_Nomutex unsigned int getId();

  private:
	Flavours studentOrder; // what the student wants to buy
	ExpType exception; // exception flag
};

#endif
