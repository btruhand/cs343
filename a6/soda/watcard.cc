#include "watcard.h"

WATCard::WATCard() : balance(0) {}

void WATCard::deposit(unsigned int amount) {
	balance+= amount; // add to balance amount many
}

unsigned int WATCard::getBalance() {
	return balance;
}

void WATCard::withdraw(unsigned int amount) {
	balance-= amount; // deduct from balance amount many
}
