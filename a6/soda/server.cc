#include "server.h"
#include "printer.h"
#include "vm.h"

NameServer::NameServer(Printer& prt, unsigned int numVendingMachines, unsigned int numStudents) : printer(prt), numVM(numVendingMachines),
	numStudents(numStudents), VMlist(new VendingMachine*[numVM]), studentCurVM(new unsigned int[numStudents]), registeredVM(0) {
	// initialize values
	for(unsigned int i = 0; i < numVM; i++) {
		VMlist[i] = NULL;
	}

	// assign students to the VMs ahead of time
	for(unsigned int i = 0; i < numStudents; i++) {
		studentCurVM[i] = i % numVM;
	}
}

void NameServer::VMregister(VendingMachine* VM) {
	VMlist[registeredVM] = VM; // register
}

VendingMachine* NameServer::getMachine(unsigned int id) {
	prevStudent = id; // pass on to main
	return VMlist[studentCurVM[id]]; // get new VM for student
}

VendingMachine** NameServer::getMachineList() {
	return VMlist;
}

void NameServer::main() {
	// start
	printer.print(Printer::NameServer, (char) Start);
	for(;;) {
		_Accept(~NameServer) {
			break;
		} or _Accept(VMregister) {
			printer.print(Printer::NameServer, (char) Register, registeredVM);
			registeredVM++;
		} or _When(registeredVM == numVM) _Accept(getMachineList) {
			// allow truck to get machine list only when all VMs have registered
			// allow truck to get the list before students get the machines
		} or _When(registeredVM == numVM) _Accept(getMachine) { 
			// allow students to come in only when all VMs have registered
			printer.print(Printer::NameServer, (char) NewVM, prevStudent, studentCurVM[prevStudent]);
			studentCurVM[prevStudent] = (studentCurVM[prevStudent] + 1) % numVM; // the next VM a student retrieves if they call getMachine again
		}
	}
	// done
	printer.print(Printer::NameServer, (char) Finished);
}

NameServer::~NameServer() {
	for(unsigned int i = 0; i < numVM; i++) {
		delete VMlist[i]; // delete the vending machines on behalf of uMain
	}
	delete [] VMlist;
	delete [] studentCurVM;
}
