#include "plant.h"
#include "printer.h"
#include "server.h"
#include "truck.h"
#include "MPRNG.h"

extern MPRNG randNum;

BottlingPlant::BottlingPlant(Printer &prt, NameServer &nameServer, unsigned int numVendingMachines,
	unsigned int maxShippedPerFlavour, unsigned int maxStockPerFlavour,
	unsigned int timeBetweenShipments) : printer(prt), server(nameServer), numVM(numVendingMachines),
	maxShipped(maxShippedPerFlavour), maxStock(maxStockPerFlavour), productionTime(timeBetweenShipments),
	produced{0,0,0,0}, shutdownFlag(false) {}

void BottlingPlant::getShipment(unsigned int cargo[]) {
	if(shutdownFlag) {
		uRendezvousAcceptor();
		_Throw Shutdown(); // shutting down, tell truck
	}
	
	// copy to cargo (truck does this because it's the truck's job to move the sodas)
	for(unsigned int i = 0; i < 4; i++) {
		cargo[i] = produced[i];
	}
}

void BottlingPlant::main() {
	Truck truck(printer, server, *this, numVM, maxStock);
	printer.print(Printer::BottlingPlant, (char) Start);
	unsigned int totalnumProduced = 0; // total number of sodas produced	
	
	for(;;) {
		// production run
		for(unsigned int i = 0; i < 4; i++) {
			produced[i] = randNum(maxShipped);
			totalnumProduced+= produced[i]; // add up
		}
		
		printer.print(Printer::BottlingPlant, (char) Generated, totalnumProduced);
		yield(productionTime); // simulate production
		_Accept(~BottlingPlant) {
			shutdownFlag = true; // SHUTTING DOWN!
			_Accept(getShipment) { // wait for truck to try to get shipment
				break;
			}
		} or _Accept(getShipment) {
			printer.print(Printer::BottlingPlant, (char) PickUp); // truck picked up the production
			totalnumProduced = 0; // reset number of sodas produced
		}
	}
	printer.print(Printer::BottlingPlant, (char) Finished);
}
