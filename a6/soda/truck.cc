#include "truck.h"
#include "printer.h"
#include "server.h"
#include "plant.h"
#include "vm.h"
#include "MPRNG.h"

extern MPRNG randNum;

Truck::Truck( Printer &prt, NameServer &nameServer, BottlingPlant &plant,
	unsigned int numVendingMachines, unsigned int maxStockPerFlavour ) : printer(prt), nameServer(nameServer),
	plant(plant), numVendingMachines(numVendingMachines), maxStockPerFlavour(maxStockPerFlavour) {}

void Truck::main() {
	VendingMachine** vmList = nameServer.getMachineList();
	printer.print(Printer::Truck, (char) Start);
	unsigned int lastVisited = numVendingMachines - 1;
	unsigned int cargo[4] = { 0,0,0,0 }; // initialize cargo to empty
	for ( ;; ) {
		yield (randNum( 1, 10 )); // take tim hortons break
		try {
			plant.getShipment(cargo); // fill in cargo from bottling plant
			unsigned int totalSodas = 0; // total sodas left in the truck
			for (unsigned int i = 0; i < 4; i++) {
				totalSodas += cargo[i];
			}
			printer.print(Printer::Truck, (char) PickedUp, totalSodas);
			if (totalSodas == 0) continue; // if production is 0, 0, 0, 0 -> then truck doesn't even attempt to go to the machines
			
			unsigned int visit = (lastVisited + 1) % numVendingMachines; // cycle through vending machines, first time starts at 0
			do {	
				// distribute to the machines
				printer.print(Printer::Truck, (char) BeginDelivery, visit, totalSodas);
				unsigned int *inventory = vmList[visit]->inventory();
				unsigned int unreplenished = 0;
				for (unsigned int i = 0; i < 4; i++) { // restock each flavour
					unsigned int diff = maxStockPerFlavour - inventory[i];
					if (diff > cargo[i]) { // check if the number of bottles needed to be restocked is greater than what is left in cargo
						unreplenished += (diff - cargo[i]);
						diff = cargo[i];
					}
					// update what is left in the cargo, add to the inventory of the machine
					totalSodas -= diff;
					inventory[i] += diff;
					cargo[i] -= diff;
				}

				if (unreplenished > 0) { // print only if some soda flavour(s) failed to be fully refilled
					printer.print(Printer::Truck, (char) Unsuccessfull, visit, unreplenished);
				}

				vmList[visit]->restocked();
				printer.print(Printer::Truck, (char) EndDelivery, visit, totalSodas);

				// visit the next machine
				visit = (visit + 1) % numVendingMachines; 
			} while ((visit != (lastVisited+1) % numVendingMachines) && (totalSodas > 0)); // stop when no more sodas or full cycle
	   		
			lastVisited = (visit-1) % numVendingMachines; // assign to what was last visited
		} catch (BottlingPlant::Shutdown) {
			break;
		}
    }
    printer.print(Printer::Truck, (char) Finished);
}
