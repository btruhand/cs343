#ifndef __PRINTER_H__
#define __PRINTER_H__

_Cormonitor Printer {
	// printer variables
	enum StateType { EMPTYSTATE = '\0' };
	struct Values {
	    int value1;
	    int value2;
	};

	unsigned int numStudents;
	unsigned int numVM;
	unsigned int numCouriers;

	// arrays of characters pertaining to what states to print
	char* studentStates;
	char* VMStates;
	char* courierStates;

	// array of values pertaining to additional information to be printed
	Values* studentValues;
	Values* VMValues;
	Values* courierValues;

	// array of characters and values to print for parent, watcard office, name server, truck and bottling plant
	char generalStates[5];
	Values generalValues[5];
	
	char printState; // state to be printed
	unsigned int index; // which to access from the array for students, VMs and couriers
	// the values to tell the printer what to print
	int printValue1;
	int printValue2;

	// private helper functions
	void printGeneral(unsigned int upto);
	void printStudents(unsigned int upto);
	void printVM(unsigned int upto);
	void printCouriers(unsigned int upto);
	void printAll(unsigned int& printUpto);

	void main();
  public:
	enum Kind { Parent, WATCardOffice, NameServer, Truck, BottlingPlant, Student, Vending, Courier };
	Printer( unsigned int numStudents, unsigned int numVendingMachines, unsigned int numCouriers );
	void print( Kind kind, char state );
	void print( Kind kind, char state, int value1 );
	void print( Kind kind, char state, int value1, int value2 );
	void print( Kind kind, unsigned int lid, char state );
	void print( Kind kind, unsigned int lid, char state, int value1 );
	void print( Kind kind, unsigned int lid, char state, int value1, int value2 );
	~Printer();
  private:
	// declarations of variables or functions that uses Kind
	Kind printing; // what kind is printing
	void InsertGeneralValues(Kind printing, bool twoValues);
};

#endif
