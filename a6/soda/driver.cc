#include "config.h"
#include "printer.h"
#include "bank.h"
#include "parent.h"
#include "office.h"
#include "server.h"
#include "vm.h"
#include "plant.h"
#include "student.h"

#include "MPRNG.h"
#include <sstream>
#include <iostream>
#include <unistd.h>
using namespace std;

// global MPRNG
MPRNG randNum;

bool convert( int &val, char *buffer ) {      // convert C string to integer
    stringstream ss( buffer );         // connect stream and buffer
    ss >> dec >> val;                   // convert integer from buffer
    return ! ss.fail() &&               // conversion successful ?
           // characters after conversion all blank ?
           string( buffer ).find_first_not_of( " ", ss.tellg() ) == string::npos;
} // convert

void usage(const char* progName) {
	cerr << progName
		<< " [ config-file [ seed (> 0) ] ]" << endl;
	exit(EXIT_FAILURE);
}

void uMain::main() {
	ConfigParms parameters;
	int seed = getpid();
	const char* configFile = "soda.config";

	Student** students; // students to be dynamically allocated
	VendingMachine** VMs; // vending machines to be dynamically allocated
	switch(argc) {
		case 3:
			if(!convert(seed, argv[2])) usage(argv[0]); // get specified seed
			if(seed <= 0) usage(argv[0]);
			// fall through
		case 2:
			configFile = argv[1];
			// fall through
		case 1:
			processConfigFile(configFile, parameters);
			break; // break
		default:
			usage(argv[0]); // more than 2 arguments given
	}

	randNum.seed(seed); // set seed
	// tasks creation
	Printer printer(parameters.numStudents, parameters.numVendingMachines, parameters.numCouriers);
	Bank bank(parameters.numStudents);
	Parent parent(printer, bank, parameters.numStudents, parameters.parentalDelay);
	WATCardOffice office(printer, bank, parameters.numCouriers);
	NameServer server(printer, parameters.numVendingMachines, parameters.numStudents);
	VMs = new VendingMachine*[parameters.numVendingMachines];
	for(unsigned int i = 0; i < parameters.numVendingMachines; i++) {
		VMs[i] = new VendingMachine(printer, server, i, parameters.sodaCost, (unsigned int) parameters.maxStockPerFlavour);
	}

	BottlingPlant plant(printer, server, parameters.numVendingMachines, parameters.maxShippedPerFlavour, parameters.maxStockPerFlavour, parameters.timeBetweenShipments);
	students = new Student*[parameters.numStudents];
	for(unsigned int i = 0; i < parameters.numStudents; i++) {
		students[i] = new Student(printer, server, office, i, parameters.maxPurchases);
	}

	// wait for the students to finish
	for(unsigned int i = 0; i < parameters.numStudents; i++) {
		delete students[i];
	}

	// clean up and termination synchronization
	delete [] students;	
	delete [] VMs;
}
