#ifndef __PLANT_H__
#define __PLANT_H__

_Cormonitor Printer;
_Task NameServer;

_Task BottlingPlant {
	enum States { Start='S', Generated='G', PickUp='P', Finished='F' };
	Printer& printer;
	NameServer& server;
	unsigned int numVM; // number of vending machines
	unsigned int maxShipped; // max shipped per flavour
	unsigned int maxStock; // max stock per flavour
	unsigned int productionTime; // timeBetweenShipments, time it takes to produce a soda

	unsigned int produced[4]; // sodas produced
	bool shutdownFlag; // tell truck to shutdown

	void main();
  public:
	_Event Shutdown {};                    // shutdown plant
	BottlingPlant( Printer &prt, NameServer &nameServer, unsigned int numVendingMachines,
		 unsigned int maxShippedPerFlavour, unsigned int maxStockPerFlavour,
		 unsigned int timeBetweenShipments );
	void getShipment( unsigned int cargo[] );
};

#endif
