#include "office.h"
#include "printer.h"
#include "bank.h"
#include "MPRNG.h"
#include "watcard.h"

extern MPRNG randNum;

WATCardOffice::Courier::Courier(unsigned int courierID, Printer& printer, Bank& bank, WATCardOffice& office) : courierID(courierID),
	printer(printer), bank(bank), office(office) {}

// courier main
void WATCardOffice::Courier::main() {
	printer.print(Printer::Courier, courierID, (char) WATCardOffice::Courier::Start);
	WATCard* card;
	unsigned int studentID;
	unsigned int amountTrans;
	WATCardOffice::Job* work;
	for(;;) {
		work = office.requestWork(); // get work
		if(work == NULL) break; // I returned but no work? Must be shutting down then
		
		studentID = (work->args).id;
		amountTrans = (work->args).amount;
		// start funds transfer
		printer.print(Printer::Courier, courierID, (char) StartTransfer, studentID, amountTrans);
		if((work->args).card == NULL) card = new WATCard; // card does not exist yet, create it
		else {
			card = (work->args).card;
			bank.withdraw(studentID, amountTrans); // withdraw from the bank
		}
		
		card->deposit(amountTrans); // deposit to card

		if(randNum(1,6) == 1) { // 1 in 6 chances to lose the card
			(work->result).exception(new WATCardOffice::Lost); // throws Lost exception
			delete card;
		} else {
			(work->result).delivery(card); // deliver the card with the new deposit
		}
		
		// end transfer
		printer.print(Printer::Courier, courierID, (char) EndTransfer, studentID, amountTrans);
		delete work; // delete the work
	}
	printer.print(Printer::Courier, courierID, (char) WATCardOffice::Courier::Finished);
}
