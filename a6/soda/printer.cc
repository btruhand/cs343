#include "printer.h"
#include <iostream>
using namespace std;

Printer::Printer(unsigned int numStudents, unsigned int numVendingMachines, unsigned int numCouriers) : numStudents(numStudents),
    numVM(numVendingMachines), numCouriers(numCouriers), studentStates(new char[numStudents]), VMStates(new char[numVM]),
	courierStates(new char[numCouriers]), studentValues(new Values[numStudents]), VMValues(new Values[numVM]),
	courierValues(new Values[numCouriers]) {
	
	// initialize values
	for(unsigned int i = 0; i < 5; i++) {
		generalStates[i] = (char)EMPTYSTATE;
		generalValues[i].value1 = 0;
		generalValues[i].value2 = 0;
	}

	for(unsigned int i = 0; i < numStudents; i++) {
		studentStates[i] = (char)EMPTYSTATE;
		studentValues[i].value1 = 0;
		studentValues[i].value2 = 0;
	}

	for(unsigned int i = 0; i < numVM; i++) {
		VMStates[i] = (char)EMPTYSTATE;
		VMValues[i].value1 = 0;
		VMValues[i].value2 = 0;
	}

	for(unsigned int i = 0; i < numCouriers; i++) {
		courierStates[i] = (char)EMPTYSTATE;
		courierValues[i].value1 = 0;
		courierValues[i].value2 = 0;
	}

	resume(); // start printer
}

// the various print routine calls based on arguments
void Printer::print(Kind kind, char state) {
	printing = kind; // set what kind (task) to be printed
	printState = state; // what state to be printed
	resume();
}

void Printer::print(Kind kind, char state, int value1) {
	printing = kind;
	printState = state;
	printValue1 = value1; // set the first value to be printed
	resume();
}

void Printer::print(Kind kind, char state, int value1, int value2) {
	printing = kind;
	printState = state;
	printValue1 = value1;
	printValue2 = value2; // set the second value to be printed
	resume();
}

void Printer::print( Kind kind, unsigned int lid, char state ) {
	printing = kind;
	printState = state;
	index = lid; // which task of the same kind to be printed e.g student 0/1/2...
	resume();
}

void Printer::print( Kind kind, unsigned int lid, char state, int value1 ) {
	printing = kind;
	printState = state;
	index = lid;
	printValue1 = value1;
	resume();
}

void Printer::print( Kind kind, unsigned int lid, char state, int value1, int value2 ) {
	printing = kind;
	printState = state;
	index = lid;
	printValue1 = value1;
	printValue2 = value2;
	resume();
}

void Printer::printGeneral(unsigned int upto) { // to print parent, office, name server, truck and plant
	for(unsigned int i = 0; i < upto; i++) {
		if(generalStates[i] != (char)EMPTYSTATE) {
			cout << generalStates[i];
			if(generalStates[i] == '.') cout << ".."; // prints ...
			else if(generalStates[i] == 'D' || generalStates[i] == 'C' || generalStates[i] == 'T' || generalStates[i] == 'N' || generalStates[i] == 'd' || generalStates[i] == 'U') {
				cout << generalValues[i].value1 << ',' << generalValues[i].value2; // print two values
			} else if(generalStates[i] == 'R' || (generalStates[i] == 'P' && i != (unsigned int) BottlingPlant) || generalStates[i] == 'G') {
				cout << generalValues[i].value1; // print one value
			}
			// reset state
			generalStates[i] = (char)EMPTYSTATE;
		}
		cout << '\t'; // print tab
	}
}

void Printer::printStudents(unsigned int upto) { // to print students
	for(unsigned int i = 0; i < upto; i++) {
		if(studentStates[i] != (char)EMPTYSTATE) {
			cout << studentStates[i];
			if(studentStates[i] == '.') cout << ".."; // prints ...
			else if(studentStates[i] == 'S') cout << studentValues[i].value1 << ',' << studentValues[i].value2;
			else if(studentStates[i] == 'V' || studentStates[i] == 'B') cout << studentValues[i].value1;
			studentStates[i] = (char)EMPTYSTATE;
		}
		cout << '\t';
	}
}

void Printer::printVM(unsigned int upto) { // to print vending machines
	for(unsigned int i = 0; i < upto; i++) {
		if(VMStates[i] != (char)EMPTYSTATE) {
			cout << VMStates[i];
			if(VMStates[i] == '.') cout << ".."; // prints ...
			else if(VMStates[i] == 'B') cout << VMValues[i].value1 << ',' << VMValues[i].value2;
			else if(VMStates[i] == 'S') cout << VMValues[i].value1;
			VMStates[i] = (char)EMPTYSTATE;
		}
		cout << '\t';
	}
}

void Printer::printCouriers(unsigned int upto) { // to print couriers
	for(unsigned int i = 0; i < upto; i++) {
		if(courierStates[i] != (char)EMPTYSTATE) {
			cout << courierStates[i];
			if(courierStates[i] == '.') cout << ".."; // prints ...
			else if(courierStates[i] == 't' || courierStates[i] == 'T') cout << courierValues[i].value1 << ',' << courierValues[i].value2;
			courierStates[i] = (char)EMPTYSTATE;
		}
		if(i != upto-1) cout << '\t'; // print tab if not last
	}
	cout << endl; // since couriers are printed last we will always print an end of line
}

void Printer::printAll(unsigned int& printUpto) {
	// do various checks and assignments so the correct printing is done
	printGeneral(printUpto >= 5 ? 5:printUpto);
	printUpto = printUpto >= 5 ? printUpto-5:0; // makes sure does not go less than 0
	printStudents(printUpto >= numStudents ? numStudents:printUpto);
	printUpto = printUpto >= numStudents ? printUpto-numStudents:0;
	printVM(printUpto >= numVM ? numVM:printUpto);
	printUpto = printUpto >= numVM ? printUpto-numVM:0;
	printCouriers(printUpto);
	printUpto = 0; // reaches 0 no matter what if we get here, last thing to print
}

void Printer::InsertGeneralValues(Kind printing, bool twoValues) {
	generalValues[printing].value1 = printValue1;
	if(twoValues) generalValues[printing].value2 = printValue2; // insert two values
}

void Printer::main() {
	// print header
	unsigned int printUpto = 0; // print up to where
	unsigned int numDone = 0; // number of finished tasks
	cout << "Parent" << '\t' << "WatOff" << '\t' << "Names" << '\t' << "Truck" << '\t' << "Plant" << '\t';

	// print students header
	for(unsigned int i = 0; i < numStudents; i++) {
		cout << "Stud" << i << '\t';
	}

	// print VMs header
	for(unsigned int i = 0; i < numVM; i++) {
		cout << "Mach" << i << '\t';
	}

	// print couriers header
	for(unsigned int i = 0; i < numCouriers; i++) {
		cout << "Cour" << i;
		if(i != numCouriers-1) cout << '\t'; // print tab if not the last one
		else cout << '\n';
	}

	unsigned int totalPrint = 5 + numStudents + numVM + numCouriers;
	for(unsigned int i = 0; i < totalPrint; i++) {
		cout << "*******";
		if(i != totalPrint-1) cout << '\t'; // print tab if not the last one
		else cout << endl;
	}

	suspend();
	
	for(;;) {
		if(printState == 'F') { // a task finished, flush buffer and print
			printAll(printUpto);
			// one more task is done
			numDone+= 1;
			// print up to the end
			printUpto = totalPrint;

			// insert . to all tasks
			for(unsigned int i = 0; i < 5; i++) {
				generalStates[i] = '.';
			}
			for(unsigned int i = 0; i < numStudents; i++) {
				studentStates[i] = '.';
			}
			for(unsigned int i = 0; i < numVM; i++) {
				VMStates[i] = '.';
			}
			for(unsigned int i = 0; i < numCouriers; i++) {
				courierStates[i] = '.';
			}
		} else { // check one by one otherwise
			if(Parent <= (unsigned int) printing && (unsigned int) printing <= BottlingPlant) {
				if(generalStates[(unsigned int) printing] != (char)EMPTYSTATE) printAll(printUpto);
			} else if(printing == Student) {
				if(studentStates[index] != (char)EMPTYSTATE) printAll(printUpto);
			} else if(printing == Vending) {
				if(VMStates[index] != (char)EMPTYSTATE) printAll(printUpto);
			} else {
				if(courierStates[index] != (char)EMPTYSTATE) printAll(printUpto);
			}
		}

		// insert appropriate values
		if(Parent <= (unsigned int) printing && (unsigned int) printing <= BottlingPlant) {
			generalStates[(unsigned int) printing] = printState;
			// collapse the conditions
			if(printState == 'D' || printState == 'C' || printState == 'T' || printState == 'N' 
				|| printState == 'd' || printState == 'U') {
				InsertGeneralValues(printing, true);
			} else if(printState == 'R' || printState == 'P' || printState == 'G') {
				InsertGeneralValues(printing, false);
			}
			
			printUpto = printUpto > (unsigned int) printing ? printUpto:(unsigned int)printing + 1; // update how much to print appropriately
		} else if(printing == Student) {
			studentStates[index] = printState;
			if(printState == 'S') {
				studentValues[index].value1 = printValue1;
				studentValues[index].value2 = printValue2;
			} else if(printState == 'V' || printState == 'B') {
				studentValues[index].value1 = printValue1;
			}

			printUpto = printUpto > ((index+1)+5) ? printUpto:((index+1)+5);
		} else if(printing == Vending) {
			VMStates[index] = printState;
			if(printState == 'S') {
				VMValues[index].value1 = printValue1;
			} else if(printState == 'B') {
				VMValues[index].value1 = printValue1;
				VMValues[index].value2 = printValue2;
			}

			printUpto = printUpto > ((index+1)+numStudents+5) ? printUpto:((index+1)+numStudents+5);
		} else {
			courierStates[index] = printState;
			if(printState == 't' || printState == 'T') {
				courierValues[index].value1 = printValue1;
				courierValues[index].value2 = printValue2;
			}

			printUpto = printUpto > ((index+1)+numVM+numStudents+5) ? printUpto:((index+1)+numVM+numStudents+5);
		}

		// check if everything is done
		if(numDone == totalPrint) break; // get out of loop
		suspend(); // wait for next resume
	}

	// print last remaining finished task
	printAll(printUpto);

	// closing off
	cout << "***********************" << endl;

	// prevent from going back to uMain
	suspend();
}

Printer::~Printer() {
	delete [] studentStates;
	delete [] VMStates;
	delete [] courierStates;

	delete [] studentValues;
	delete [] VMValues;
	delete [] courierValues;
}
