#include "vm.h"
#include "watcard.h"
#include "server.h"
#include "printer.h"

VendingMachine::VendingMachine(Printer& prt, NameServer& nameServer, unsigned int id, unsigned int sodaCost,
		unsigned int maxStockPerFlavour) : printer(prt), nameServer(nameServer), VMid(id), sodaCost(sodaCost),
		maxStock(maxStockPerFlavour), sodaStock{0,0,0,0} {}

void VendingMachine::buy(Flavours flavour, WATCard& card) {
	// pass the flavour and soda
	studentCard = &card;
	studentOrder = flavour;
	studentWait.wait(); // wait for soda
	if(exception == OUTOFSTOCK) {
		_Throw Stock();
	} else if(exception == OUTOFFUNDS) {
		_Throw Funds();
	}
}

unsigned int* VendingMachine::inventory() {
	return sodaStock;
}

// signal vending machine it has been restocked
void VendingMachine::restocked() {}

unsigned int VendingMachine::cost() {
	return sodaCost;
}

unsigned int VendingMachine::getId() {
	return VMid;
}

void VendingMachine::main() {
	nameServer.VMregister(this); // register myself
	printer.print(Printer::Vending, VMid, (char) Start, sodaCost);
	for(;;) {
		_Accept(~VendingMachine) {
			break;
		} or _Accept(inventory) { // truck should be able to reload first before any students attempting to buy
			printer.print(Printer::Vending, VMid, (char) Reload); // truck is reloading
			_Accept(restocked) { // wait for truck to finish reloading
				printer.print(Printer::Vending, VMid, (char) CompleteReload); // finish reloading if we got her
			}
		} or _Accept(buy) {
			if(sodaStock[(unsigned int) studentOrder] == 0) { // no soda
				exception = OUTOFSTOCK;
			} else if(studentCard->getBalance() < sodaCost) { // or not enough funds
				exception = OUTOFFUNDS;
			} else {
				exception = NOEXP; // no exception to be thrown
				// withdraw money
				studentCard->withdraw(sodaCost);
				// decrement number of soda in stock and print
				printer.print(Printer::Vending, VMid, (char) Bought, (unsigned int) studentOrder, --sodaStock[(unsigned int) studentOrder]);
			}
			studentWait.signalBlock(); // soda may be ready, or something weird happened
		}
	}
	printer.print(Printer::Vending, VMid, (char) Finished);
}
