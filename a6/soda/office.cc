#include "office.h"
#include "bank.h"
#include "printer.h"
#include <iostream>
using namespace std;

WATCardOffice::WATCardOffice(Printer& prt, Bank& bank, unsigned int numCouriers) : printer(prt), bank(bank),
	numCouriers(numCouriers), myCouriers(new Courier*[numCouriers]){

	for(unsigned int i = 0; i < numCouriers; i++) {
		myCouriers[i] = new Courier(i, printer, bank, *this);
	}
}

WATCardOffice::~WATCardOffice() {
    for(unsigned int i = 0; i < numCouriers; i++) {
		delete myCouriers[i];
    }

    delete [] myCouriers;
}

WATCard::FWATCard WATCardOffice::create(unsigned int sid, unsigned int amount) {
	// make the work
	Args arguments; // job arguments
	arguments.id = sid;
	arguments.amount = amount;
	arguments.card = NULL;
	prevArgs = arguments; // pass over to main

	Job* newWork = new Job(arguments);
	workRequests.push(newWork);
	toPrint = CreateDone; // indicate create call is finished
	return newWork->result;
}

WATCard::FWATCard WATCardOffice::transfer(unsigned int sid, unsigned int amount, WATCard* card) {
	// make the work
	Args arguments; // job arguments
	arguments.id = sid;
	arguments.amount = amount;
	arguments.card = card;
	prevArgs = arguments; // pass over to main

	Job* newWork = new Job(arguments);
	workRequests.push(newWork);
	toPrint = TransferDone; // indicate transfer call is finished
	return newWork->result;
}

WATCardOffice::Job* WATCardOffice::requestWork() {
	if(workRequests.empty()) return NULL; // still empty? then must be shutting down
	return workRequests.front();
}

//office main
void WATCardOffice::main() {
	printer.print(Printer::WATCardOffice, WATCardOffice::Start);
	for(;;) {
		_Accept(~WATCardOffice) {
			break; //terminate
		} or _When(!workRequests.empty()) _Accept(requestWork) { // accept couriers only when there is work
			// allow couriers to take work first before more work is put
			printer.print(Printer::WATCardOffice, (char) RequestDone);
			workRequests.pop(); // take out the work that was just taken
		} or _Accept(create,transfer) {
			printer.print(Printer::WATCardOffice, (char) toPrint, prevArgs.id, prevArgs.amount);
		}
	}

	// make cleaning up couriers possible
	for(unsigned int i = 0; i < numCouriers; i++) {
		_Accept(requestWork) {}
	}

	printer.print(Printer::WATCardOffice, WATCardOffice::Finished);
}
