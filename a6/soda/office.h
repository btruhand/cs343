#ifndef __OFFICE_H__
#define __OFFICE_H__

#include "watcard.h"
#include <queue>

_Cormonitor Printer;
_Monitor Bank;

_Task WATCardOffice {
	enum State {Start = 'S', RequestDone = 'W', CreateDone = 'C', TransferDone = 'T', Finished = 'F'};
	struct Args {
		unsigned int id;
		unsigned int amount;
		WATCard* card;
	};

	struct Job {                           // marshalled arguments and return future
		Args args;                         // call arguments (YOU DEFINE "Args")
		WATCard::FWATCard result;          // return future
		Job( Args args ) : args( args ) {}
	};

	_Task Courier {
		enum CourierState {Start = 'S', StartTransfer = 't', EndTransfer = 'T', Finished = 'F'};
		unsigned int courierID;
		Printer& printer;
		Bank& bank;
		WATCardOffice& office;

	  public:
		Courier(unsigned int courierID, Printer& printer, Bank& bank, WATCardOffice& office);
		void main();
	};                 // communicates with bank

	Printer& printer;
	Bank& bank;
	unsigned int numCouriers;
	Courier** myCouriers;
	    
	std::queue<Job*> workRequests; // queue of jobs
	Args prevArgs; // record what were the arguments for the work just created
	State toPrint; // indicate which state to print

	void main();
  public:
	_Event Lost {};                        // lost WATCard
	WATCardOffice( Printer &prt, Bank &bank, unsigned int numCouriers );
	WATCard::FWATCard create( unsigned int sid, unsigned int amount );
	WATCard::FWATCard transfer( unsigned int sid, unsigned int amount, WATCard *card );
	Job* requestWork();
	~WATCardOffice();
};

#endif
