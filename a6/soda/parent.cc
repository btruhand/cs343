#include "parent.h"
#include "printer.h"
#include "bank.h"
#include "MPRNG.h"

// Used for randomly selecting a dollar amount and a student to give the money to.
extern MPRNG randNum;

// Parent Constructor.
Parent::Parent( Printer &prt, Bank &bank, unsigned int numStudents, unsigned int parentalDelay ) : printer(prt), bank(bank),
	numStudents(numStudents), parentalDelay(parentalDelay) {}

void Parent::main() {
	printer.print(Printer::Parent, (char) Start);		// print start on main
	for (;;) {
		_Accept (~Parent) {			// The parent is busy waiting for the call to its destructor to know when to terminate.
		    	break;
		} _Else {						// To avoid blocking on this call, using a terminating Else on the accept statement.
			yield (parentalDelay);				// Before each gift transfer, yield for a non-random time.
			unsigned int amount = randNum( 1, 3 );		// Give a random amount of money [$1, $3] to a random student.
			unsigned int id = randNum( numStudents - 1 );
			printer.print(Printer::Parent, (char) Deposit, id, amount);
			bank.deposit(id, amount);			// deposit after printing
		}
	}
	printer.print(Printer::Parent, (char) Finished);
}
