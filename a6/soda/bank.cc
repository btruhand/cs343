#include "bank.h"
#include "printer.h"

Bank::Bank(unsigned int numStudents) : numStudents(numStudents), waitWithdrawal(new uCondition[numStudents]),
	accounts(new unsigned int[numStudents]), requestAmount(new unsigned int[numStudents]) {
	// initialize values
	for(unsigned int i = 0; i < numStudents; i++) {
		accounts[i] = 0;
		requestAmount[i] = 0;
	}
}

void Bank::deposit(unsigned int id, unsigned int amount) {
	accounts[id]+= amount;
	if(accounts[id] >= requestAmount[id]) waitWithdrawal[id].signal(); // signal the courier if there is one
}

void Bank::withdraw(unsigned int id, unsigned int amount) {
	requestAmount[id] = amount;
	if(accounts[id] < amount) waitWithdrawal[id].wait(); // wait until the necessary amount comes
	
	// deduct the necessary amount
	accounts[id]-= amount;
}

Bank::~Bank() {
	delete [] waitWithdrawal;
	delete [] accounts;
	delete [] requestAmount;
}
