#ifndef __SERVER_H__
#define __SERVER_H__

_Task VendingMachine;
_Cormonitor Printer;

_Task NameServer {
	enum State { Start = 'S', Register = 'R', NewVM = 'N', Finished = 'F'};
	Printer& printer;
	unsigned int numVM;
	unsigned int numStudents;
	
	VendingMachine** VMlist;
	unsigned int* studentCurVM;
	unsigned int registeredVM; // number of VMs that have registered
   
	unsigned int prevStudent; // previous student that came
	void main();
  public:
	NameServer( Printer &prt, unsigned int numVendingMachines, unsigned int numStudents );
	void VMregister( VendingMachine *vendingmachine );
	VendingMachine *getMachine( unsigned int id );
	VendingMachine **getMachineList();
	~NameServer();
};

#endif
