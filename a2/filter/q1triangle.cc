#include "q1triangle.h"
#include <iostream>
using namespace std;

Triangle::Triangle(Filter* f, int basewidth) : f(f), // check for even width
    _basewidth(basewidth+(basewidth+1)%2) {}

Triangle::~Triangle() {
    delete f;
}

void Triangle::process(int printnum) {
    for(int i = 0; i < printnum; i++) {
	if(ch == '\n' || ch == '\t') ch = ' '; // replace tabs and newlines with a space
	f->put(ch);
	suspend();
	terminate_if_EOF();
    }

    terminate_if_EOF(); // needed if there is no more input
}

void Triangle::main() {
    try {
	terminate_if_EOF(); // needed if there is no input AT ALL
	int middle;
	// the middle of the triangle and also the number of lines to be printed
	int height = middle = _basewidth/2+1;
	for(;;) {

	    for(int i = 0; i < height; i++) {
		// print spaces
		for(int j = 1; j+i < middle; j++) {
		    f->put(' ');
		}

		// print the left characters
		process(i);
		
		// print the middle character
		process(1);

		// print the right characters
		process(i);

		// if we get here then we finished one pyramid
	    	f->put('\n');
	    }
	}
    } catch(EndOfFile) {
	f->put(ch);
    }
}
