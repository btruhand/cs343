#ifndef __FILTER_H__
#define __FILTER_H__

_Coroutine Filter {
  protected:
    static const unsigned char End_Filter = '\377';
    
    unsigned char ch;

    // used for Throw/Catch
    struct EndOfFile {};
    void terminate_if_EOF(void);
    
  public:
    void put( unsigned char c ) {
        ch = c;
        resume();
    }

    virtual ~Filter(); 
};

#endif
