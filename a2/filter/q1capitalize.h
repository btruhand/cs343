#ifndef __CAPITALIZE_H__
#define __CAPITALIZE_H__
#include "q1filter.h"

_Coroutine Capitalize : public Filter {
    private:
	// a pointer to the other filters
    	Filter* f;void process();
	void main();
    // YOU MAY ADD PRIVATE MEMBERS
  public:
    Capitalize(Filter *f);
    ~Capitalize();
};

#endif
