#include "q1whitespace.h"
#include <iostream>
using namespace std;

Whitespace::Whitespace(Filter* f) : f(f) {}

Whitespace::~Whitespace() {
    delete f;
}

void Whitespace::process(void) {
    f->put(ch);
    suspend();
}

void Whitespace::main() {
    try {
        terminate_if_EOF(); // needed if there is no input AT ALL
        for(;;) {
            while(isblank(ch)) {
                // while the character are spaces or tabs just don't send them down the filter
                suspend();
            }

            terminate_if_EOF();
            // we got to the end of the line already
            if(ch == '\n') {
                process();
                continue;
            }

            line:
            for(;;) {
		// loop while the character is not a space or a tab
		while(!isblank(ch)) {
                    terminate_if_EOF();
                    if(ch != '\n') {
                        process();
                    } else {
                        process();
                        break line; // if we see a newline then that indicates we'll be reading a new line
                    }
                }
                
                while(isblank(ch)) suspend(); // suspend while we keep seeing a space or a tab

                if(ch == '\n') { // checks if we're at the end of the line
                    process();
                    break; // newline so go to the outer loop
                }
                
                f->put(' '); // send a single space
                terminate_if_EOF();
           }
        }
    } catch (EndOfFile) {
	    f->put(ch);
    }
}    
