#include "q1filter.h"
#include <iostream>
using namespace std;

void Filter::terminate_if_EOF(void) {
    if(ch == End_Filter) throw EndOfFile();
}

// empty destructor
Filter::~Filter() {}
