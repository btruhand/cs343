#ifndef __READER_H__
#define __READER_H__

#include "q1filter.h"
#include <iostream>

_Coroutine Reader : public Filter {
  private:
    // a pointer to the other filters
    Filter* f;
    std::istream* in;
    void main();
    // YOU MAY ADD PRIVATE MEMBERS
  public:
    Reader( Filter *f, std::istream *i );
    ~Reader();
};

#endif
