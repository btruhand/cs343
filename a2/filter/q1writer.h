#ifndef __WRITER_H__
#define __WRITER_H__

#include "q1filter.h"
#include <iostream>

_Coroutine Writer : public Filter {
    private:
	std::ostream* out;
	void main();
    // YOU MAY ADD PRIVATE MEMBERS
  public:
    Writer( std::ostream *o );
    ~Writer();
};

#endif
