#include <sstream>
#include <iostream>
#include <iomanip>
#include <ctype.h>
using namespace std;

int main() {
    char ch = '\n';
    stringstream ss;
    ss << setw(2) << hex << (unsigned int)(unsigned char)ch;
    ss >> ch;
    cout << ch;
    ss >> ch;
    cout << ch;
}
