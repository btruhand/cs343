#ifndef __WHITESPACE_H__
#define __WHITESPACE_H__

#include "q1filter.h"

_Coroutine Whitespace : public Filter {
    private:
	// a pointer to the other filters
	Filter* f;
	void process(void);
	void main();
    // YOU MAY ADD PRIVATE MEMBERS
  public:
    Whitespace(Filter *f);
    ~Whitespace();
};

#endif
