#include "q1filter.h"
#include "q1reader.h"
#include "q1writer.h"
#include "q1triangle.h"
#include "q1capitalize.h"
#include "q1hexdump.h"
#include "q1whitespace.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
using namespace std;

bool convert( int &val, char *buffer ) {		// convert C string to integer
    stringstream ss( buffer );			// connect stream and buffer
    ss >> dec >> val;					// convert integer from buffer
    return ! ss.fail() &&				// conversion successful ?
    // characters after conversion all blank ?
       string( buffer ).find_first_not_of( " ", ss.tellg() ) == string::npos;
    // convert
}

void usage(char* argv[]) {
    cerr << "Usage: " << argv[0]
	<< " [ -filter-options ] "
	<< " [ infile [ outfile ] ]" << endl;
    exit(EXIT_FAILURE);
}

void delete_stream(istream* in, ostream* out) {
    if(in != &cin) delete in;
    if(out != &cout) delete out;
}

void uMain::main() {
    // input and output stream
    istream* infile = &cin;
    ostream* outfile = &cout;

    int loopargv;
    int optpos[argc]; //option position array
    int index = 0;
    for(loopargv = 1; loopargv < argc; loopargv++) {
	string argument(argv[loopargv]);
	if(argument == "-h" || argument == "-s" || argument == "-w"
		|| argument == "-T") {
	    optpos[index] = loopargv;
	    index++;
	    if(argument == "-T") loopargv++;
	} else if(argument[0] == '-') {
	    cerr << "Unknown filter " << argument << endl;
	    exit(EXIT_FAILURE);
	} else break;
    }

    if(loopargv < argc - 2) { // incorrect number of arguments
	usage(argv);
    }

    if(loopargv == argc) goto filter;

    if(loopargv < argc) {
    	try {
	    infile = new ifstream(argv[loopargv]);
	} catch(uFile::Failure) {
	    cerr << "Error! Could not open input file \"" << argv[loopargv] << "\"" << endl;
	    exit(EXIT_FAILURE);
	}
    }

    if(loopargv+1 == argc-1) { //an output file has been given
	try {
	    outfile = new ofstream(argv[loopargv+1]);
	} catch (uFile::Failure) {
	    cerr << "Error! Could not open output file \"" << argv[loopargv+1] << "\"" << endl;
	    delete infile;
	    exit(EXIT_FAILURE);
	}
    }

filter:
    // create Writer
    Filter* filter = new Writer(outfile);


    for(loopargv = index-1; loopargv >= 0; loopargv--) {
	int filter_pos = optpos[loopargv];
	string option(argv[filter_pos]);

	if(option == "-h") {
	    filter = new Hexdump(filter);
	} else if(option == "-s") {
	    filter = new Capitalize(filter);
	} else if(option == "-w") {
	    filter = new Whitespace(filter);
	} else if(option == "-T") {
	    int basewidth;
	    // check for missing argument
	    if(filter_pos+1 == argc) {
		cerr << "-T requires an argument" << endl;
		// delete current filters
		delete filter;
		exit(EXIT_FAILURE);
	    }
	    
	    // convert argument to integer
	    convert(basewidth, argv[filter_pos+1]);
	    filter = new Triangle(filter, basewidth);
	}
    }

    // Create the Reader, which also starts the pipeline
    filter = new Reader(filter, infile);
    // once we got here input has finished
    delete filter;
}
