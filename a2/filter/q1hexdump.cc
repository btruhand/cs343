#include "q1hexdump.h"
#include <sstream>
#include <iomanip>
using namespace std;

Hexdump::Hexdump(Filter* f) : f(f) {}

Hexdump::~Hexdump() {
    delete f;
}

void Hexdump::process() {
    for(int i = 0; i < 2; i++) {
	stringstream ss;
	terminate_if_EOF();
	ss << setfill('0');
	ss << setw(2) << hex << (unsigned int)(unsigned char)ch;
	while(ss >> ch) {
	    f->put(ch);
	}
	suspend();
    }
}

void Hexdump::wrapup_process() {
    process();

    // check if the sentinel value has been sent
    terminate_if_EOF();
    // send the space to the next filter
    f->put(' ');

    process();
    
    terminate_if_EOF();
}

void Hexdump::main() {
    try {
	terminate_if_EOF(); // needed if there is no input AT ALL 
	for(;;) {
	    for(int i = 0; i < 3; i++) { // loop four times then print and send a newline
		wrapup_process();
		for(int i = 0; i < 3; i++) {
		    f->put(' ');
		}
	    }

	    // do a final processing
	    wrapup_process();

	    // print the newline
	    f->put('\n');
	}
    } catch(EndOfFile) {
	// tell the other filters to terminate
	f->put(ch);
    }
}
