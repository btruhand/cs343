#include "q1writer.h"
#include <iostream>
#include <iomanip>
using namespace std;

Writer::Writer(ostream* o) : out(o) {}

void Writer::main() {
    try {
	for(;;) {
	    terminate_if_EOF();
	    *out << ch;
	    suspend();
	}
    } catch(EndOfFile) {}
}

Writer::~Writer() {
    if(out != &cout) delete out;
}
