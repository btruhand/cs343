#include "q1reader.h"
#include <iostream>
using namespace std;

Reader::Reader(Filter* f, istream* i) : f(f), in(i) {
    // start the coroutine
    resume();
}

void Reader::main() {
    *in >> noskipws;
    while(*in >> ch) {
	f->put(ch);
    }
    // if we get here then we've reached EOF
    f->put(End_Filter);
}

Reader::~Reader() {
    if(in != &cin) delete in;
    delete f;
}
