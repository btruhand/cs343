#ifndef __HEXDUMP_H__
#define __HEXDUMP_H__

#include "q1filter.h"

_Coroutine Hexdump : public Filter {
    private:
	// a pointer to the other filters
	Filter* f;
	void process();
	void wrapup_process();
	void main();
    // YOU MAY ADD PRIVATE MEMBERS
  public:
    Hexdump(Filter *f);
    ~Hexdump();
};

#endif
