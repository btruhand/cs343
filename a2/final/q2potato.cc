#include "q2potato.h"
#include <iostream>
#include <stdlib.h>
using namespace std;


void Potato::startset(unsigned int maxTicks) {
    ticks = rand()%maxTicks + 1;
    cout << "  POTATO will go off after " << ticks;
    if(ticks == 1) cout << " toss" << endl;
    else cout << " tosses" << endl;
}

Potato::Potato(unsigned int maxTicks) {
    // set the timer
    startset(maxTicks);
}

void Potato::reset(unsigned int maxTicks) {
    // set a new timer
    startset(maxTicks);
}

bool Potato::countdown() {
    // decrement ticks then check if 0
    if(--ticks == 0) return true;
    return false;
}
