#include "q1capitalize.h"
#include <ctype.h> // for toupper and isspace

Capitalize::Capitalize(Filter* f) : f(f) {}

Capitalize::~Capitalize() {
    delete f;
}

void Capitalize::process() {
    f->put(ch);
    suspend();
}

void Capitalize::main() {
    try {
        terminate_if_EOF(); // needed if there is no input AT ALL
        
        while(isspace(ch)) {
            process();
        }
        
	if(ch != '.' && ch != '?' && ch != '!') {
	    // we saw the first character that is not a whitespace so try to capitalize it
	    ch = toupper(ch);
	    process();
	}

        for(;;) {
            while(ch != '.' && ch != '?' && ch != '!') {
                process();
            }

            terminate_if_EOF();
            // print the '.' or '?' or '!'
            process();

            terminate_if_EOF();
            if(!isspace(ch)) continue; // since we didn't see any spaces, it's not a new sentence

            // we saw a whitespace and now we're waiting for a non-whitespace
            while(isspace(ch)) {
                process();
            }

            terminate_if_EOF();
            
            // if we get here we saw a character that is not a whitespace
            if(ch != '.' && ch != '?' && ch != '!') {
		ch = toupper(ch); // capitalize it if it's capitalizeable
            	process();
	    }
        }
    } catch(EndOfFile) {
        // tell the other filters to end
        f->put(ch);
    }
}
