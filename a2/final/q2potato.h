#ifndef __POTATO_H__
#define __POTATO_H__

class Potato {
    private:
	unsigned int ticks;

	void startset(unsigned int maxTicks);
    public:
    	Potato( unsigned int maxTicks = 10 );
    	void reset( unsigned int maxTicks = 10 );
    	bool countdown();
};

#endif
