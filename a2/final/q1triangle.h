#ifndef __TRIANGLE_H__
#define __TRIANGLE_H__

#include "q1filter.h"

_Coroutine Triangle : public Filter {
    private:
	// a pointer to the other filters
	Filter* f;
	int _basewidth;
	
	void process(int printnum);
	void main();
    // YOU MAY ADD PRIVATE MEMBERS
  public:
    Triangle(Filter *f, int basewidth);
    ~Triangle();
};

#endif
