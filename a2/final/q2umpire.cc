#include "q2umpire.h"
#include "q2potato.h"
#include <iostream>
#include <stdlib.h>
using namespace std;

Umpire::Umpire(Player::PlayerList& players) : players(players), discard_player(0), setnum(1) {}

void Umpire::set(unsigned int player) {
    discard_player = player;
    resume();
}

unsigned int Umpire::ChoosePlayer() {
    return rand()%players.size();
}

void Umpire::main() {
    // first time we enter the game
    cout << players.size() << " players in the match" << endl;

    // create the hotpotato on the heap so that it can be moved across stacks
    Potato hotpotato;

    cout << "Set " << setnum << ":\tU";
    players[ChoosePlayer()]->toss(hotpotato);
    // if we get back here then we've made a cycle, but then that means the potato exploded
    
    while(1) {
        Player::PlayerList::iterator it;
        for(it = players.begin(); it != players.end(); it++) {
            if(discard_player == (*it)->getId()) break; // found our player
        }

        players.erase(it); // erase the player from the list
        cout << " is eliminated\n";

        if(players.size() == 1) break; // somebody has won

        hotpotato.reset(); // reset hotpotato timer

        setnum++;
        cout << "Set " << setnum << ":\tU";
        players[ChoosePlayer()]->toss(hotpotato);
    }

    cout << players[0]->getId() << " wins the Match!" << endl;
}
