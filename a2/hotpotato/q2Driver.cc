#include "q2player.h"
#include "q2umpire.h"
#include <time.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
using namespace std;

bool convert( int &val, char *buffer ) {		// convert C string to integer
    stringstream ss( buffer );			// connect stream and buffer
    ss >> dec >> val;					// convert integer from buffer
    return ! ss.fail() &&				// conversion successful ?
    // characters after conversion all blank ?
       string( buffer ).find_first_not_of( " ", ss.tellg() ) == string::npos;
    // convert
}

void usage(char* argv[]) {
    cerr << "Usage: " << argv[0]
	<< " number-of-players (2-20 inclusive) "
	<< " [ seed ]" << endl;
    exit(EXIT_FAILURE);
}

void uMain::main() {
    int numPlayers;
    int* seed = NULL;
    switch(argc) {
	case 3:
	    seed = new int;
	    convert(*seed, argv[2]);
	case 2:
	    if(seed != NULL) srand(*seed);
	    else srand(time(NULL));
	    
	    convert(numPlayers, argv[1]);
	    if(numPlayers >= 2 && numPlayers <= 20) break;
	default:
	    if(seed != NULL) delete seed;
	    usage(argv);
    }

    Player::PlayerList players(numPlayers, NULL);
    Player::PlayerList copyOfPlayers(numPlayers, NULL); // needed for deletion later
    Umpire umpire(players);

    for(int i = 0; i < numPlayers; i++) {
	players[i] = new Player(umpire, i, players);
	copyOfPlayers[i] = players[i];
    }

    umpire.set(0);
    // if we got here then the game has ended

    for(unsigned int j = 0; j < copyOfPlayers.size(); j++) {
	delete copyOfPlayers[j];
    }

    if(seed != NULL) delete seed;
}
