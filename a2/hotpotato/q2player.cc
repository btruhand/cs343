#include "q2player.h"
#include "q2potato.h"
#include"q2umpire.h"
#include <vector>
#include <stdlib.h>
#include <iostream>
using namespace std;

Player::Player(Umpire& umpire, unsigned int Id, PlayerList& players) : umpire(umpire), id(Id), players(players), hotpotato(NULL) {}

unsigned int Player::getId() {
    return id;
}

void Player::main() {
    int me;
    int next;
    while(1) {
	int numPlayersLeft = players.size();
	// find where I am in the list
	for(me = 0; me < numPlayersLeft; me++) {
	    if(id == players[me]->getId()) break;
	}

	// choose a player to toss to
	do {
	    next = rand() % numPlayersLeft;
	} while ( next == me );

	players[next]->toss(*hotpotato);
    }
}


void Player::toss(Potato& potato) {
    cout << " -> " << id; // print information
    if(potato.countdown()) umpire.set(getId()); // tell the umpire I lost
    else {
	hotpotato = &potato; // pass the potato to the coroutine
	resume(); // go to coroutine main
    }
}
