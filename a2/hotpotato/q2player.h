#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "q2potato.h"
#include <vector>

_Coroutine Umpire;

_Coroutine Player {
    void main();
  public:
    typedef std::vector<Player*> PlayerList; // container type of your choice
    Player( Umpire &umpire, unsigned int Id, PlayerList &players );
    unsigned int getId();
    void toss( Potato &potato );

  private:
    Umpire& umpire;
    unsigned int id;
    PlayerList& players;
    Potato* hotpotato;
};

#endif
