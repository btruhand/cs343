#ifndef __UMPIRE_H__
#define __UMPIRE_H__

#include "q2player.h"

_Coroutine Umpire {
  private:
      Player::PlayerList& players;
      unsigned int discard_player;
      unsigned int setnum;

      unsigned int ChoosePlayer();
      void main();
  public:
      Umpire( Player::PlayerList &players );
      void set( unsigned int player );
};

#endif
