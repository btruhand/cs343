#include "q2printer.h"
#include "q2tallyVotes.h"
#include "q2voter.h"

TallyVotes::TallyVotes(unsigned int group, Printer& printer) : signalFlag(false), group(group), printer(printer), FinalBallot(0), waiting(group) {} 

bool TallyVotes::vote(unsigned int id, bool ballot) {
    printer.print(id, Voter::Vote, ballot); 
    FinalBallot+= ballot; // increase ballot

    waiting--; // decrease the number of group members we have to wait

    if(waiting == 0) {
	signalFlag = true;
	printer.print(id, Voter::Complete);
    }

    WAITUNTIL(signalFlag, printer.print(id, Voter::Block, group-waiting), printer.print(id, Voter::Unblock, group-waiting-1));
    
    waiting++;
    // return the result to the voter, 0 if FinalBallot is less than or half of the group 1 otherwise
    bool returnBallot = (FinalBallot <= group/2) ? 0 : 1;

    // if this is the last voter to get out, reset for the next group
    if(waiting == group) {
	FinalBallot = 0; signalFlag = false;
    }

    RETURN(returnBallot);
}
