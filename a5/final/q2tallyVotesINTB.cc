#include "q2tallyVotes.h"
#include "q2printer.h"
#include "q2voter.h"
#include <iostream>
using namespace std;

TallyVotes::TallyVotes(unsigned int group, Printer& printer) : serving(0), ticket(0), bargeFlag(false), signalFlag(false), group(group), printer(printer), FinalBallot(0), waiting(group) {} 

void TallyVotes::wait() {
    bench.wait();                              // wait until signalled
    while ( rand() % 5 == 0 ) {                // multiple bargers allowed
        _Accept( vote ) {                      // accept barging callers
        } _Else {                              // do not wait if no callers
        } // _Accept
    } // while
}

void TallyVotes::signalAll() {                 // also useful
    while ( ! bench.empty() ) bench.signal();  // drain the condition
}

bool TallyVotes::vote(unsigned int id, bool ballot) {
    if(bargeFlag) {
	unsigned int myTicket = ++ticket;
	printer.print(id, Voter::Barging);
	while(myTicket != serving) { // while I'm not yet served
	    wait();
	}
    }

    printer.print(id, Voter::Vote, ballot); 
    FinalBallot+= ballot; // increase ballot

    waiting--; // decrease the number of group members we have to wait
    if(waiting != 0) { // while still waiting for others
	printer.print(id, Voter::Block, group-waiting); // print number of voters waiting
	if(serving != ticket) { // if there are bargers, one of which is going to be my group mate, wake my group mate
	    serving++;
	    signalAll();
	} else bargeFlag = false; // reset barge flag since nobody to wake up

	while(!signalFlag) wait(); // while I'm not supposed to wake up yet
	printer.print(id, Voter::Unblock, group-waiting-1); // print number of voters still waiting
    } else printer.print(id, Voter::Complete); // last group member has came

    waiting++;
    // return the result to the voter, 0 if FinalBallot is less than or half of the group 1 otherwise
    bool returnBallot = (FinalBallot <= group/2) ? 0 : 1;

    // if this is the last voter to get out, reset for the next group
    // and wake up first voter of next group possibly
    if(waiting == group) {
	signalFlag = false;
	FinalBallot = 0;
	if(ticket != serving) {
	    serving++;
	    signalAll();
	} else bargeFlag = false;
    } else if (waiting == 1) { // the first one to get here
	bargeFlag = true; // prevent bargers
	signalFlag = true; // tell my group mates to wake up
	signalAll(); // signal my group members
    }
    
    return returnBallot;
}
