#ifndef __VOTER_H__
#define __VOTER_H__

#include "q2tallyVotes.h"

// Forward declaration
_Cormonitor Printer;

_Task Voter {
    private:
	unsigned int id;
	TallyVotes& tallier;
	Printer& printer;

	void main();
    public:
	enum States { Start = 'S', Vote = 'V', Block = 'B', Unblock = 'U', Barging = 'b',
			       Complete = 'C', Finished = 'F' };
	Voter( unsigned int id, TallyVotes &voteTallier, Printer &printer );
};

#endif
