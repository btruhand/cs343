#define AUTOMATIC_SIGNAL uCondition waitGroup

#define WAITUNTIL(pred, before, after) if(!(pred)) { \
    while(!waitGroup.empty()) waitGroup.signal(); \
	do { \
	before; \
	waitGroup.wait(); \
	after; \
    } while(!(pred)); \
}

#define RETURN(expr) while(!waitGroup.empty()) waitGroup.signal(); \
    return expr;
