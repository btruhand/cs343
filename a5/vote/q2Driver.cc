#include "MPRNG.h"
#include "q2printer.h"
#include "q2tallyVotes.h"
#include "q2voter.h"
#include <iostream>
#include <sstream>
#include <ctime>
using namespace std;

// global MPRNG to be used by voters
MPRNG randGen;

bool convert( int &val, char *buffer ) {		// convert C string to integer
    std::stringstream ss( buffer );			// connect stream and buffer
	    ss >> dec >> val;					// convert integer from buffer
		    return ! ss.fail() &&				// conversion successful ?
				// characters after conversion all blank ?
					string( buffer ).find_first_not_of( " ", ss.tellg() ) == string::npos;
} // convert

void usage(char* progname) {
	cerr << "Usage: " << progname
	     << " [ V (> 0 V mod G = 0)"
	     << " [ G (> 0 G is odd)"
	     << " [ seed (> 0) ] ] ]" << endl;
	exit(EXIT_FAILURE);
}

void uMain::main() {
	int V = 6;
	int G = 3;
	int seed = time(NULL);
	switch(argc) {
		case 4:
		    if(!convert(seed, argv[3])) usage(argv[0]);
		    if(seed <= 0) usage(argv[0]); // less than or equal to 0
		case 3:
		    if(!convert(G, argv[2])) usage(argv[0]);
		    if(G <= 0 || G % 2 == 0) usage(argv[0]); // less than or equal to 0 or even
		case 2:
		    if(!convert(V, argv[1])) usage(argv[0]);
		    if( V <= 0 || V % G != 0) usage(argv[0]); // less than or equal to 0 or not a multiple of G
		case 1:
		    break;
		default:
		    usage(argv[0]); // too many arguments
	}

	randGen.seed(seed); // set MPRNG seed

	// setup
	Printer printer(V);
	TallyVotes tallier(G, printer);
	
	Voter* voters[V];
	for(int i = 0; i < V; i++) {
	    voters[i] = new Voter(i, tallier, printer);
	}

	// cleanup
	for(int i = 0; i < V; i++) {
	    delete voters[i];
	}
}
