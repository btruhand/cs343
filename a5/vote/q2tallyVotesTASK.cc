#include "q2tallyVotes.h"
#include "q2printer.h"
#include "q2voter.h"


TallyVotes::TallyVotes(unsigned int group, Printer& printer) : voteMade(0), voterID(0), returnBallot(0), group(group), printer(printer), FinalBallot(0), waiting(group) {} 

bool TallyVotes::vote(unsigned int id, bool ballot) {
    printer.print(id, Voter::Vote, ballot); 
    voterID = id;
    voteMade = ballot;

    waiting--; // indicate entrance
    waitGroup.wait(); // wait for others or wait for tally voter to do its job
    printer.print(id, Voter::Unblock, group-waiting-1); // print number of voters still waiting

    waiting++; // indicate exit
    return returnBallot;
}

void TallyVotes::main() {
    for(;;) {
	_Accept(~TallyVotes) { // end the task
	    break;
	} or _Accept(vote) {
	    printer.print(voterID, Voter::Block, group-waiting); // specify that this voter blocked
	    FinalBallot+= voteMade;
	    
	    if(waiting == 0) { // the group has been formed
		returnBallot = (FinalBallot <= group/2) ? 0:1; // calculate result
		printer.print(voterID, Voter::Complete); // signify the process is complete
		while(!waitGroup.empty()) waitGroup.signalBlock(); // wake every voter up cause everything's done
		FinalBallot = 0; // reset for a new group
	    }
	}
    }
}
