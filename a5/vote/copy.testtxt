This documentation provides the testing that has been done on the programs. The tests are broken down to the following:

1. General testing
	- Command arguments
	- Printing format
	- Randomness and similarity due to seed
2. Barging testing
3. Large input testing

General Testing
===============

Command Arguments
-----------------

These test consists of checking whether the driver program correctly trears malformed arguments. At the same time it
also checks to see if the program correctly recognizes correct input. Since it is only checking the driver program
we will only use one implementation, since it would be the same case with any of the implementation

     
Default value test
******************

This is the run of the test:

     2	btruhand@ubuntu1204-006:~/cs343/a4/vote$ ./voteMC > out
     3	btruhand@ubuntu1204-006:~/cs343/a4/vote$ exit
     4	exit

And this is the output:

     1	Voter0	Voter1	Voter2	Voter3	Voter4	Voter5
     2	=======	=======	=======	=======	=======	=======
     3	S	S	S	S	S	S
     4	V 1
     5	B 1	V 0
     6		B 2		V 1
     7				C
     8	...	...	...	F 1	...	...
     9	U 1
    10	F 1	...	...	...	...	...
    11		U 0
    12	...	F 1	...	...	...	...
    13					V 0
    ... 

Here we are checking if the program works correctly when no arguments are provided. We should have 6 voters and a group
of 3 tourists per tour. It is clear from the above output this is what we get and so the program passes this test.

Malformed input
***************

Here are the run of the tests:

     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 19284s.d2
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit
     
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 12 2838.da
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit
     
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 15 5 129da.
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit

This test checks whether or not the program handles malformed input correctly. That is when either V, G or seed is not
an integer. The above output shows that the program handles malformed input correctly by printing a usage message.

Incorrect number of voters
**************************

Here are the run of the tests:

We are testing whether or not the program identifies incorrect values of voters and print an appropriate usage message.

     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC -1 3 129 > out
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit
     
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 0 3 129 > out
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit
     
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 7 3 129 > out
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit

     1	Script started on Wed 05 Nov 2014 01:38:30 AM EST
     2	btruhand@ubuntu1204-006:~/cs343/a4/vote$ ./voteMC 7 > out
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-006:~/cs343/a4/vote$ exit
     5	exit
     6	
     7	Script done on Wed 05 Nov 2014 01:38:38 AM EST

Here we are checking if the program prints a proper usage message in the case of when the number of voters supplied is
not correct, that is it is not a multiple G or greater than 0. Indeed that is the case as showin in the above test.
The first three demonstrates cases where the value of V is not correct but G (number of groups) is correct. Hence
we can conclude that the error message was printed because of the incorrectness of the value of V (the number of
voters). The last test checks that even when G is not specified, hence takes on its default value 3 then if the number
of voters is not a multiple of G, like 7, the program still outputs a usage message properly.

Incorrect value for G
*********************

Here is the run of the tests:

     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 12 4
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit
     
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 12 -1
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit
     
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 12 0
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit

The value of G should be an odd number that is greater than 0. The above tests check whether or not the program
identifies when this requirement is violated or not and clearly it does. We conclude then that program behaves
correctly (by printing a usage message) when it sees such a case.

Incorrect value for seed
************************

Here is the run of the test:

     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 12 1 -129
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit
     
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 12 1 0
     3	Usage: ./voteMC [ V (> 0 V mod G = 0) [ G (> 0 G is odd) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     5	exit

The value of seed should be a positive integer and here we check whether the program handles the case when the seed
specified is not a positive integer. Note that the value of V and G are both legitimate in both tests, hence the usage
message is printed because of the negative value of the seed. Hence the program behaves correctly.

Printing Format
---------------

Here we want to test whether or not the program has the correct print format. Though the Printer used in all
implementations are the same, how they are used might be different and may lead to differences or even incorrect
behaviour. We will run this test by using all three implementations on the same command arguments

Here is the run of the tests each with their respective outputs:

     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 9 3 1284 > out
     3	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     4	exit
     
     1	Voter0	Voter1	Voter2	Voter3	Voter4	Voter5	Voter6	Voter7	Voter8
     2	=======	=======	=======	=======	=======	=======	=======	=======	=======
     3	S	S	S	S	S	S	S	S	S
     4	V 1
     5	B 1	V 1
     6		B 2	V 1
     7			C
     8	...	...	F 1	...	...	...	...	...	...
     9		U 1
    10	...	F 1	...	...	...	...	...	...	...
    11	U 0
    12	F 1	...	...	...	...	...	...	...	...
    13								V 0
    14							V 1	B 1
    15							B 2		V 0
    16									C
    17	...	...	...	...	...	...	...	...	F 0
    18							U 1
    19	...	...	...	...	...	...	F 0	...	...
    20								U 0
    21	...	...	...	...	...	...	...	F 0	...
    22				V 0
    23				B 1		V 0
    24					V 0	B 2
    25					C
    26	...	...	...	...	F 0	...	...	...	...
    27						U 1
    28	...	...	...	...	...	F 0	...	...	...
    29				U 0
    30	...	...	...	F 0	...	...	...	...	...	
    31	=================
    32	All tours started
     
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteSEM 9 3 1284 > out
     3	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     4	exit
     
     1	Voter0	Voter1	Voter2	Voter3	Voter4	Voter5	Voter6	Voter7	Voter8
     2	=======	=======	=======	=======	=======	=======	=======	=======	=======
     3	S	S	S	S	S	S	S	S	S
     4	V 1
     5	B 1	V 1
     6		B 2	V 1
     7			C
     8	...	...	F 1	...	...	...	...	...	...
     9	U 1
    10	F 1	...	...	...	...	...	...	...	...
    11		U 0
    12	...	F 1	...	...	...	...	...	...	...
    13								V 0
    14							V 1	B 1
    15							B 2		V 0
    16									C
    17	...	...	...	...	...	...	...	...	F 0
    ...
    31	=================
    32	All tours started
     
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteBAR 9 3 1284 > out
     3	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     4	exit
     
     1	Voter0	Voter1	Voter2	Voter3	Voter4	Voter5	Voter6	Voter7	Voter8
     2	=======	=======	=======	=======	=======	=======	=======	=======	=======
     3	S	S	S	S	S	S	S	S	S
     4	V 1
     5	B 1	V 1
     6		B 2	V 1
     7			C
     8	...	...	F 1	...	...	...	...	...	...
     9		U 1
    10	...	F 1	...	...	...	...	...	...	...
    11	U 0
    12	F 1	...	...	...	...	...	...	...	...
    13								V 0
    14							V 1	B 1
    15							B 2		V 0
    16									C
    17	...	...	...	...	...	...	...	...	F 0
    ...
    31	=================
    32	All tours started

We note that for all these tests the number of voters is 9 and the number of members in a group is 3. The ellipsis
for some of the output of the tests are there because the remaining output are the same or is very similary with the
of the output of invoking voteMC as ./voteMC 9 3 1284. 

Since there are 9 tourists and the size of a group is 3, we should have 3 groups formed and based on the first test
it is indeed the case that we have 3 groups formed by seeing that there are exactly three of consecutive prints of Fs
(spaced out by a print of unblocking of course). 

We know that a V and F is followed with the ballot of the voter and the final ballot respectively. It would seem to be the
case whenever a V or F is printed. We note also that the final ballot printed based on the ballots of the voters in the
group are correct. We can see this by simply counting the number of 0s and 1s made by the voters in a group, and
whether the dominant vote is the one that is printed besides F. We note also that for every row that has an F printed,
it is indeed the case that all other columns aside from the column that has F has ellipses which is what we want.

It seems to also be the case that unblocking and blocking are printed correctly. We see this by observing that the
first voter blocked always prints B 1, indicating there are 1 person (itself) waiting and the next one B 2, indicating
that there is itself and the first blocker waiting. When unblocking, the first one to unblock always prints U 1,
properly indicating that there is someone still waiting and the next one U 0 which says no one else is blocking. Since
the last person to vote in the group never blocks (in virtue of the voter being the last voter in the group), the
program correctly never prints a B nor a U for such a voter, but instead a C since the voter completes the necessary
number of votes needed.

Lastly we note that the spacing requirement and the layout of the Voters0, Voters1, Voters2 and the underscores seem to
follow the requirement. We conclude then that the program has the correct printing format for all implementations.

Randomness and Similarity of output due to seed
-----------------------------------------------

These are simple tests to see that the same seed for small input should produce identical or closely similar outputs.
In contrast random seed (achieved by not specifying the seed in the command argument) should produce quite different
outputs even with the same input. Since all implementations use the same pseudo-random number generator, we would only
test using one implementation

Here are the run of the tests for the similarity case:

     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 6 3 1924 > out
     3	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 6 3 1924 > out2
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 6 3 1924 > out3
     5	btruhand@ubuntu1204-002:~/cs343/a4/vote$ diff out out2
     6	btruhand@ubuntu1204-002:~/cs343/a4/vote$ diff out out3
     7	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 15 3 129842 > out
     8	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 15 3 129842 > out2
     9	btruhand@ubuntu1204-002:~/cs343/a4/vote$ diff out out2
    10	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
    11	exit

We see that for small inputs the same seed produce the same input. It also does not seem to matter what the seed is
since both using 1924 or 129842 as seeds produce the same output for sufficiently small input. We generalize then
that for any sufficiently small input and any seed, the program(s) output similar results as what we desire.

Here are the run of the tests for the random case:

     1	Script started on Wed 05 Nov 2014 02:51:25 AM EST
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 18 3 > out
     3	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 18 3 > out2
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ diff out out2
     5	3c3,23
     6	< S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S
     7	---
     8	> S	S	S	S	S	S	S	S	S
     9	> 								V 1
    10	> 								B 1	S	S	S
    11	> 											V 0
    12	> 											B 2	S	S	S	S
    13	> 															V 0
    14	> 															C
    15	> ...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	F 0	...	...
    16	> 								U 1								S	S
    17	> ...	...	...	...	...	...	...	...	F 0	...	...	...	...	...	...	...	...	...
    18	> 											U 0
    19	> ...	...	...	...	...	...	...	...	...	...	...	F 0	...	...	...	...	...	...
    20	> 			V 1
    21	> 			B 1										V 1
    22	> 					V 0								B 2
    23	> 					C
    24	> ...	...	...	...	...	F 1	...	...	...	...	...	...	...	...	...	...	...	...
    25	> 			U 1
    26	> ...	...	...	F 1	...	...	...	...	...	...	...	...	...	...	...	...	...	...
   ... (more diff output)

   113	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./vo51
   114	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 55 11 > out2
   115	btruhand@ubuntu1204-002:~/cs343/a4/vote$ diff out out2
   116	3,15c3,20
   117	< S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S
   118	< 																																														V 1
   119	< 																								V 0																						B 1	S	S	S	S	S	S	S	S
   120	< 														V 1										B 2
   121	< 								V 0						B 3
   122	< 								B 4																							V 1
   123	< 																															B 5										V 1
   124	< 							V 1																																		B 6
   125	< 							B 7																		V 0
   126	< 																									B 8																	V 1
   127	< 																																										B 9	V 1
   128	< 																																											B 10							V 0
   129	< 																																																		C
   130	---
   131	> S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S	S
   132	> 																																										V 1
   133	> 																																										B 1	S	S	S	S	S	S	S	S
   134	> 																																																		V 1
   135	> 		V 0																																																B 2	S	S	S	S
   136	> 		B 3							V 1
   137	> 									B 4												V 0
   138	> 							V 1														B 5
   139	> 							B 6												V 0
   140	> 																			B 7																					V 0
   ... (more diff output)

We see that when random seeds are given even with the same command arguments for V and G, whether they are 'small' or
'big', the program(s) produce varying results as seen by the output of diff.

By checking these cases we can say that the program does correctly uses the seed argument. That is when it is specified
the program uses the specified seed while when it isn't it uses a random source for the seed. This behaviour is what
we desire.

Barging Testing
===============

Though difficult I will try to give a basic account as to whether or not my programs have barging or not. Note that due to
space constraint I will only use small inputs.

These are the run of the tests with their respective output:

     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 12 3 12834 > out
     3	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     4	exit
     
     1	Voter0	Voter1	Voter2	Voter3	Voter4	Voter5	Voter6	Voter7	Voter8	Voter9	Voter10	Voter11
     2	=======	=======	=======	=======	=======	=======	=======	=======	=======	=======	=======	=======
     3	S	S	S	S	S	S	S	S	S	S	S	S
     4			V 1
     5			B 1									V 1
     6		V 1										B 2
     7		C
     8	...	F 1	...	...	...	...	...	...	...	...	...	...
     9			U 1
    10	...	...	F 1	...	...	...	...	...	...	...	...	...
    11												U 0
    12	...	...	...	...	...	...	...	...	...	...	...	F 1
    13									V 0
    14					V 1				B 1
    15					B 2		V 0
    16							C
    17	...	...	...	...	...	...	F 0	...	...	...	...	...
    18									U 1
    19	...	...	...	...	...	...	...	...	F 0	...	...	...
    20					U 0
    21	...	...	...	...	F 0	...	...	...	...	...	...	...
    22	V 1
    23	B 1										V 0
    ...
         
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteSEM 12 3 12834 > out
     3	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     4	exit
     1	Voter0	Voter1	Voter2	Voter3	Voter4	Voter5	Voter6	Voter7	Voter8	Voter9	Voter10	Voter11
     2	=======	=======	=======	=======	=======	=======	=======	=======	=======	=======	=======	=======
     3	S	S	S	S	S	S	S	S	S	S	S	S
     4			V 1
     5			B 1									V 1
     6		V 1										B 2
     7		C
     8	...	F 1	...	...	...	...	...	...	...	...	...	...
     9			U 1
    10	...	...	F 1	...	...	...	...	...	...	...	...	...
    11												U 0
    12	...	...	...	...	...	...	...	...	...	...	...	F 1
    13									V 0
    14					V 1				B 1
    15					B 2		V 0
    16							C
    17	...	...	...	...	...	...	F 0	...	...	...	...	...
    18									U 1
    19	...	...	...	...	...	...	...	...	F 0	...	...	...
    20					U 0
    21	...	...	...	...	F 0	...	...	...	...	...	...	...
    22	V 1
    23	B 1										V 0
    ...
         
     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteBAR 12 3 12834 > out
     3	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit
     4	exit
     
     1	Voter0	Voter1	Voter2	Voter3	Voter4	Voter5	Voter6	Voter7	Voter8	Voter9	Voter10	Voter11
     2	=======	=======	=======	=======	=======	=======	=======	=======	=======	=======	=======	=======
     3	S	S	S	S	S	S	S	S	S	S	S	S
     4			V 1
     5			B 1									V 1
     6		V 1										B 2
     7		C
     8	...	F 1	...	...	...	...	...	...	...	...	...	...
     9												U 1
    10	...	...	...	...	...	...	...	...	...	...	...	F 1
    11			U 0
    12	...	...	F 1	...	...	...	...	...	...	...	...	...
    13									V 0
    14					V 1				B 1
    15					B 2		V 0
    16							C
    17	...	...	...	...	...	...	F 0	...	...	...	...	...
    18					U 1
    19	...	...	...	...	F 0	...	...	...	...	...	...	...
    20									U 0
    21	...	...	...	...	...	...	...	...	F 0	...	...	...
    22	V 1
    23	B 1										V 0
    ...

Ellipses are used for brevity. Based ont he above output for all implementations it doesn't seem to be the case that
there are bargers. We see this since there are no 'spoilt' final ballots. If bargers were possible, it would be the
case that the bargers' vote would count to the group's ballot. Thus those voters that are legitimately in the same
group will see the total ballot that was affected by the bargers vote and may print out the incorrect vote when
printing F. That is if 1 was the dominant vote for that group and the barger vote a 0, the behaviour of the program
or simply the voters of that group that were woken up would be undefined since then the number of votes of 1s and 0s
may possibly be tied, which is not within what the voters should consider. Another way of seeing that there are no
bargers is by noticing that there are no prints of W (some voter waiting) after some voter prints a C that is when a
voter has given the last vote for its group, but before all the voters finish (get out of the tally vote signified by
an unblock and finishing). Prints of W in this case would signify that a voter went in and started voting for its own
group, which should not be allowed since that voter would be a barger. Though technically for larger inputs there may
be a time when some voter comes in after the last voter of the group gets out but before the voter gets to print F.
In this case this voter won't be a barger since all voters of the group have gone out properly. In none of the above
output do the above mentioned cases happen. We will conclude then that the programs are free of barging.
     
Large Input Testing
===================

Here is the run of the test:

     2	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 550 5 > out
     3	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 550 5 > out
     4	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 550 5 > out
     5	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteSEM 550 5 > out
     6	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteSEM 550 5 > out
     7	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteSEM 550 5 > out
     8	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteBAR 550 5 > out
     9	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteBAR 550 5 > out
    10	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteBAR 550 5 > out
    11	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteMC 1331 11 > out
    12	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteBAR 1331 11 > out
    13	btruhand@ubuntu1204-002:~/cs343/a4/vote$ ./voteSEM 1331 11 > out
    14	btruhand@ubuntu1204-002:~/cs343/a4/vote$ exit

Due to brevity's sake the output are not shown. However the test simply checks whether the programs work for
relatively larger inputs. The idea is that since the programs are by nature non-deterministic, it might be that
problems, related to hanging up of the program or crashing, in the program are concealed because of non-determinism.
By having larger input we hope to see if we are able to catch those problems by extending the time it takes to finish
the program. It appears however based on the above test that we see at least for the tests above that the program seems
to work fine, that is it doesn't hang or crash. Though the above tests are small in amount, similar kinds of tests have
been done and none of which causes a problem.
