#include "q2printer.h"
#include <iostream>
using namespace std;

Printer::Printer(unsigned int voters) : numVoters(voters) {
	resume(); // start coroutine
}

void Printer::main() {
    unsigned int maxID = 0;
    unsigned int doneVoters = 0;
    char voterStates[numVoters]; // store voter states here
    unsigned int voterMiscData[numVoters];

    // print the layout and initialize voterStates
    for(unsigned int i = 0; i < numVoters; i++) {
	voterStates[i] = '\0';
	cout << "Voter" << i;
	if(i != numVoters-1) cout << '\t'; // if not last iteration
    }
    cout << endl;

    for(unsigned int i = 0; i < numVoters; i++) {
	cout << "=======";
	if(i != numVoters-1) cout << '\t'; // if not last iteration
    }
    cout << endl;

    suspend();

    printformat:
    for(;;) {
	if(voterStates[curvoterID] != '\0' || curvoterState == Voter::Finished) {
	    // if something was stored in the same slot before or the state is Finished, then flush
	    for(unsigned int i = 0; i <= maxID; i++) {
		if(voterStates[i] != '\0') {
		    cout << voterStates[i];
		    if(voterStates[i] == '.') cout << ".."; // print "..." along witht eh above
		    if(voterStates[i] == Voter::Block || voterStates[i] == Voter::Unblock || 
			    voterStates[i] == Voter::Finished || voterStates[i] == Voter::Vote) cout << " " << voterMiscData[i];
		    
		    voterStates[i] = '\0'; // flush
		}

		if(i != maxID) cout << '\t';
	    }

	    cout << endl;
	    maxID = 0;

	    // print the user has finished
	    if(curvoterState == Voter::Finished) {
		for(unsigned int i = 0; i < numVoters; i++) {
		    if(i != curvoterID) voterStates[i] = '.';
		    else voterStates[i] = curvoterState;
		}
		if(++doneVoters == numVoters) break;
		maxID = numVoters-1;
	    }
	}

	// save states
	if(curvoterID > maxID) maxID = curvoterID; // save up to where we need to print
	voterStates[curvoterID] = curvoterState;
	if(curvoterState == Voter::Block || curvoterState == Voter::Unblock 
		|| curvoterState == Voter::Finished || curvoterState == Voter::Vote) voterMiscData[curvoterID] = miscData;

	suspend(); // wait until the next print
    }

    // if we get here then we know the last voter has finished, so print finished for this voter and ... for the rest
    for(unsigned int i = 0; i < numVoters; i++) {
	if(i != curvoterID) cout << "...";
	else cout << "F " << miscData;

	cout << '\t';
    }

    cout << "\n=================\n" << "All tours started" << endl;
   
    // needed so that we won't return to uMain, printer's cocaller
    suspend();
}

void Printer::assign(unsigned int id, Voter::States state) {
    curvoterID = id;
    curvoterState = state;
}

void Printer::print(unsigned int id, Voter::States state) {
    assign(id, state);
    resume();
}

void Printer::print(unsigned int id, Voter::States state, bool vote) {
    assign(id, state);
    miscData = vote;
    resume();
}

void Printer::print(unsigned int id, Voter::States state, unsigned int numBlocked) {
    assign(id, state);
    miscData = numBlocked;
    resume();
}
