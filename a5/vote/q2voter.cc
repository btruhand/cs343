#include "MPRNG.h"
#include "q2voter.h"
#include "q2printer.h"

// global MPRNG to be used by voters
extern MPRNG randGen;

Voter::Voter(unsigned int id, TallyVotes& voteTallier, Printer& printer) : id(id), tallier(voteTallier), printer(printer) {}

void Voter::main() {
    // print start
    printer.print(id, Start); 

    // yield for 0 to 19 times
    yield(randGen(19));
    // Generate random value, 0 or 1
    bool ballot = randGen(1);

    // tell printer I'm voting with this ballot
    //printer.print(id, Vote, ballot); 
    
    // start voting
    ballot = tallier.vote(id, ballot);

    // tell printer voting is done and this is the vote I got
    printer.print(id, Finished, ballot);
} 
