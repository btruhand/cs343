#ifndef __TALLYVOTES_H__
#define __TALLYVOTES_H__

// Forward declaration of Printer
_Cormonitor Printer;

#if defined( IMPLTYPE_EXT )                    // external scheduling monitor solution
// includes for this kind of vote-tallier
_Monitor TallyVotes {
    // private declarations for this kind of vote-tallier
#elif defined( IMPLTYPE_INT )                  // internal scheduling monitor solution
// includes for this kind of vote-tallier
_Monitor TallyVotes {
    // private declarations for this kind of vote-tallier
    uCondition waitGroup;
#elif defined( IMPLTYPE_INTB )                 // internal scheduling monitor solution with barging
// includes for this kind of vote-tallier
_Monitor TallyVotes {
    // private declarations for this kind of vote-tallier
    uCondition bench;                          // only one condition variable (you may change the variable name)
    unsigned int serving;
    unsigned int ticket;
    bool bargeFlag;
    bool signalFlag;
    void wait();                               // barging version of wait
    void signalAll();                          // unblock all waiting tasks
#elif defined( IMPLTYPE_AUTO )                 // automatic-signal monitor solution
// includes for this kind of vote-tallier
#include "AutomaticSignal.h"

_Monitor TallyVotes {
    // private declarations for this kind of vote-tallier
    AUTOMATIC_SIGNAL;
    bool signalFlag;
#elif defined( IMPLTYPE_TASK )                 // internal/external scheduling task solution
_Task TallyVotes {
    // private declarations for this kind of vote-tallier
    void main();
    uCondition waitGroup;
    bool voteMade;
    unsigned int voterID;
    unsigned int returnBallot;
#else
    #error unsupported voter type
#endif
    // common declarations
    unsigned int group;
    Printer& printer;
    unsigned int FinalBallot;
    unsigned int waiting;
  public:                                     // common interface
    TallyVotes( unsigned int group, Printer &printer );
    bool vote( unsigned int id, bool ballot );
};

#endif
