Here is the result for running the program with the -t option with size 100000000 and depth 0:
9.46u 0.30s 0:09.77

Notice that the user time and real time is almost proportional to each other. That is there is not much difference
between them. Hence though predominantly the time taken to run the program is taken up for running the user program
there is a slight overhead, most likely from time slicing, thus the slight difference between the two times.

Tests
===== 
Note that the following results are produced by running the following script:

#!/bin/bash

/usr/bin/time -f "%Uu %Ss %E" ./mergesort -t 100000000 0
/usr/bin/time -f "%Uu %Ss %E" ./mergesort -t 100000000 1
/usr/bin/time -f "%Uu %Ss %E" ./mergesort -t 100000000 2
/usr/bin/time -f "%Uu %Ss %E" ./mergesort -t 100000000 3
/usr/bin/time -f "%Uu %Ss %E" ./mergesort -t 100000000 4
/usr/bin/time -f "%Uu %Ss %E" ./mergesort -t 100000000 5
/usr/bin/time -f "%Uu %Ss %E" ./mergesort -t 100000000 6

Depth 0
-------
9.54u 0.33s 0:09.87

Depth 1
-------
11.25u 0.30s 0:06.30

Depth 2
-------
13.80u 0.29s 0:06.68

Depth 3
-------
15.06u 0.39s 0:06.87

Depth 4
-------
14.66u 0.38s 0:06.98

Depth 5
-------
14.57u 0.35s 0:07.00

Depth 6
-------
14.67u 0.44s 0:06.94

As an additional note, this test was conducted on a system with 4 CPUs

Observed behaviour
========================
We take note that a depth of 1 makes the real time significantly lower than if we run the program with depth 0. Notice
also that as the depth increases, real time increases (in general) but user time decreases from depth 4 onwards (again
in general).

Explanation of behaviour
========================
The reason why the real time significantly decreases from depth 0 to depth 1 is most likely because the program now is
able to be parallelized on two different CPUs as depth 1 creates another kernel thread along with the original kernel
thread that spawned the top level task. Thus less actual time is used to do the work. The reason why the real time
increases as the depth increases is probably because the amount of time that a single thread gets on a CPU is decreased
due to there being more threads hence there are more time slices which incurs cost due to context switching. Similarly
the decrease in user time can be seen as a result of there being more context switches happening.
