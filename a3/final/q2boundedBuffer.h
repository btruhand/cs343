#ifndef __BOUNDEDBUFFER_H__
#define __BOUNDEDBUFFER_H__
#include "MPRNG.h"

template<typename T> class BoundedBuffer {
    private:
	const unsigned int size; // size of the buffer
	unsigned int numElem; // keep track of the number of elements in the buffer
	unsigned int takeFront; // keep track of where we should take the element from
	unsigned int insertBack; // keep track of where we should insert an element to
	T* buffer; // the buffer
	bool bargeFlag; // flag to indicate bargers to stop
	
	// locks needed
	uOwnerLock ownerlk;
	uCondLock prodcondlk;
	uCondLock conscondlk;
	uCondLock bargelk;
    public:
	BoundedBuffer( const unsigned int size = 10 );
	void insert( T elem );
	T remove();
	~BoundedBuffer();
};

template<typename T> BoundedBuffer<T>::~BoundedBuffer() { delete [] buffer; }

#ifdef BUSY                            // busy waiting implementation
// implementation

template<typename T> BoundedBuffer<T>::BoundedBuffer(const unsigned int size) : size(size), numElem(0), takeFront(0),
    		insertBack(0), buffer(new T[size]) {}

template<typename T> void BoundedBuffer<T>::insert(T elem) {
    // get the owner lock, block if not possible
    ownerlk.acquire();
    while(numElem == size) {
	// the buffer's full gotta wait till somebody consumes
	prodcondlk.wait(ownerlk);
    }

    assert(numElem != size); // check that buffer is not full
    buffer[insertBack] = elem;
    insertBack = (insertBack + 1) % size;
    numElem++;

    if(!conscondlk.empty()) conscondlk.signal(); // signal if there is a consumer being blocked
    ownerlk.release();
}

template<typename T> T BoundedBuffer<T>::remove() {
    T ret;
    // get the owner lock, block if not possible
    ownerlk.acquire();
    while(numElem == 0) {
	// the buffer's empty gotta wait till somebody produces
	conscondlk.wait(ownerlk);
    }

    assert(numElem != 0); // check that buffer is not empty
    ret = buffer[takeFront];
    takeFront = (takeFront + 1) % size;
    numElem--;

    if(!prodcondlk.empty()) prodcondlk.signal(); // signal if there is a produced being blocked
    ownerlk.release();
    return ret;
}


#endif // BUSY

#ifdef NOBUSY                          // no busy waiting implementation
// implementation

template<typename T> BoundedBuffer<T>::BoundedBuffer(const unsigned int size) : size(size), numElem(0), 
    		takeFront(0), insertBack(0), buffer(new T[size]), bargeFlag(false) {}

template<typename T> void BoundedBuffer<T>::insert(T elem) {
    // get the owner lock, block if not possible
    ownerlk.acquire();

    if(bargeFlag) {
	// somebody was signaled, don't barge
	bargelk.wait(ownerlk);
    }

    if(numElem == size) {
	// the buffer's full gotta wait till somebody consumes
	if(!bargelk.empty()) bargelk.signal(); // there were bargers blocked, wake one up
	else bargeFlag = false;
	prodcondlk.wait(ownerlk);
    }

    assert(numElem != size); // check that the buffer is not full
    buffer[insertBack] = elem;
    insertBack = (insertBack + 1) % size;
    numElem++;

    // check if a consumer was blocked to start consuming
    if(!conscondlk.empty()) {
	bargeFlag = true;
	conscondlk.signal(); // signal if there is a consumer being blocked
    } else if (!bargelk.empty()) bargelk.signal();
    else bargeFlag = false;
    
    ownerlk.release();
}

template<typename T> T BoundedBuffer<T>::remove() {
    T ret;
    // get the owner lock. block if not possible
    ownerlk.acquire();
    
    if(bargeFlag) {
	// somebody was signaled, don't barge
	bargelk.wait(ownerlk);
    }

    if(numElem == 0) {
	// the buffer's empty gotta wait till somebody produces
	if(!bargelk.empty()) bargelk.signal(); // there were bargers blocked, wake one up
	else bargeFlag = false;

	conscondlk.wait(ownerlk);
    }

    assert(numElem != 0); // check that the buffer is not empty
    ret = buffer[takeFront];
    takeFront = (takeFront + 1) % size;
    numElem--;
    
    // check if a producer was blocked to start producing
    if(!prodcondlk.empty()) {
	bargeFlag = true;
	prodcondlk.signal(); // signal if there is a produced being blocked
    } else if (!bargelk.empty()) bargelk.signal();
    else bargeFlag = false;

    ownerlk.release();
    return ret;
}


#endif // NOBUSY

#endif
