#ifndef __MERGESORT_H__
#define __MERGESORT_H__

#include <iostream>
using namespace std;

template<typename T> _Task Mergesort {
    private:
        void merge(unsigned int p1low, unsigned int p1high, unsigned int p2low, unsigned int p2high);
        void packetSort(unsigned int low, unsigned int high);
        void sort(unsigned int low, unsigned int high);
        void main();

        // private constructor for child tasks
        Mergesort(T* values, unsigned int low, unsigned int high, unsigned int depth, T* copyArray); 

        T* values;
        unsigned int low;
        unsigned int high;
        unsigned int depth;
        T* copyArray; 
        bool topLevel;

    public:
        Mergesort( T values[], unsigned int low, unsigned int high, unsigned int depth );
};

// Implementation in header file because Mergesort is a template class

template<typename T> Mergesort<T>::Mergesort(T values[], unsigned int low, unsigned int high, unsigned int depth, T* copyArray) : values(values), low(low),
            high(high), depth(depth), copyArray(copyArray), topLevel(false) {}


template<typename T> Mergesort<T>::Mergesort(T values[], unsigned int low, unsigned int high, unsigned int depth) : values(values), low(low), high(high), depth(depth),
            copyArray(new T[high-low+1]), topLevel(true) {}

template<typename T> void Mergesort<T>::merge(unsigned int p1low, unsigned int p1high, unsigned int p2low, unsigned int p2high) { 
    int tmp = p1low; 
    for(unsigned int i = p1low; i <= p2high; i++) {
        if(p1low <= p1high && (p2low > p2high || values[p1low] <= values[p2low])) {
            copyArray[i] = values[p1low];
            p1low++;
        } else {
            copyArray[i] = values[p2low];
            p2low++;
        }
    }

    // copy over the result from copyArray
    for(unsigned int i = tmp; i <= p2high; i++) {
        values[i] = copyArray[i];
    }
}

template<typename T> void Mergesort<T>::packetSort(unsigned int low, unsigned int high) {
}

template<typename T> void Mergesort<T>::sort(unsigned int low, unsigned int high) {
    verify();

    unsigned int middle = low + (high-low)/2;
    unsigned int numElem = high-low+1;
    if(numElem > 2) { // create two partitions and sort them
        sort(low,middle);
        sort(middle+1,high);
        merge(low, middle, middle+1, high);
    } else { // number of elements is 2 or less so no need to create partition
        if(numElem == 2) {
            if(values[low] > values[high]) {
                int temp = values[high];
                values[high] = values[low];
                values[low] = temp;
            }
        }
        return;
    }

}

template<typename T> void Mergesort<T>::main() {
    unsigned int middle = low + (high-low)/2; // avoiding overflow
    if(depth > 0) { // then we create child Task
        Mergesort<T> childSort(values, low, middle, depth-1, copyArray);
        sort(middle+1, high); // does the sort on the other half sequentially
    } else { // sequantially sort
        sort(low, middle);
        sort(middle+1, high);
    }
  
    // merge everything together needed here in case of concurrent sorting
    merge(low, middle, middle+1, high);

    // only the top level task should deallocate copyArray
    if(topLevel) delete copyArray;
}

#endif
