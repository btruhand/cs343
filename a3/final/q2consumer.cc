#include "q2consumer.h"
#include "MPRNG.h"

extern MPRNG randomGen;

Consumer::Consumer( BoundedBuffer<int> &buffer, const int Delay, const int Sentinel, int &sum ) : buffer(buffer), delay(Delay),
    		sentinel(Sentinel), sum(sum) {}


void Consumer::main() {
    for(;;) {
	// yield for some random number of times, from 0 to delay-1
	yield(randomGen(0, delay-1));

	int removed = buffer.remove(); // remove element from the buffer
	if(removed == sentinel) break; // end the consumer

	sum+= removed;
    }
}
