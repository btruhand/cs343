#ifndef __PRODUCER_H__
#define __PRODUCER_H__
#include "q2boundedBuffer.h"

_Task Producer {
    private:
	BoundedBuffer<int>& buffer; // the buffer to insert to
	const int produce; // number of things to produce
	const int delay; // used to randomly yield some number of times, up to delay-1

	void main();
    public:
	Producer( BoundedBuffer<int> &buffer, const int Produce, const int Delay );
};

#endif
