#include "q1mergesort.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;

unsigned int uDefaultStackSize() {
    return 512 * 1000;        // set task stack-size to 512K
}

bool convert( int &val, char *buffer ) {      // convert C string to integer
    stringstream ss( buffer );         // connect stream and buffer
    ss >> dec >> val;                   // convert integer from buffer
    return ! ss.fail() &&               // conversion successful ?
           // characters after conversion all blank ?
           string( buffer ).find_first_not_of( " ", ss.tellg() ) == string::npos;
} // convert

void usage() {
    cerr << "Usage:\n"
         << "mergesort -s unsorted-file-name [ sorted-file ]\n"
         << "OR\n"
         << "mergesort -t size (>= 0) [ depth (>= 0) ]" << endl; 
    exit( EXIT_FAILURE );              // TERMINATE
} // usage

void printArray(TYPE* values, unsigned int numElem, ostream* outFile) {
    for(unsigned int i = 0; i < numElem;) {
        // print up to 22 elements or until the end of the list
        for(int j = 0 ; j < 22 && i < numElem; j++) {
            *outFile << values[i] << " ";
            i++;
        }
      
        *outFile << endl; 
        if((i%22) == 0) *outFile << "  "; //printed 22 elements already
    }
}

void uMain::main() {
    bool print;
    int depth = 0; // default value
    int size;

    istream* inFile = &cin;
    ostream* outFile = &cout;
    string command;

    if(argc == 1) usage();
    else command = argv[1];

    if(command == "-s") print = true;
    else if(command == "-t") print = false;
    else usage();

    switch(argc) {
        case 4:
            if(print) {
                try {
                    outFile = new ofstream(argv[3]);
                } catch(uFile::Failure) {
                    cerr << "Error! Unable to open output file \"" << argv[3] << "\"" << endl;
                    usage();
                }
            } else {
                if(!convert(depth, argv[3]) || depth < 0) usage();
            }
        case 3:
           if(print) {
               try {
                   inFile = new ifstream(argv[2]);
               } catch(uFile::Failure) {
                   cerr << "Error! Unable to open input file \"" << argv[2] << "\"" << endl;
               }
           } else {
               if(!convert(size, argv[2]) || size < 0) usage();
           }
           break;
        case 2:
           usage();
    }

    uProcessor p[(1 << depth) -1] __attribute__((unused)); // 2^depth - 1 kernel threads

    if(print) {
        TYPE* values;
        unsigned int numElem;
        while(*inFile >> numElem) {
            if(numElem == 0) {
                *outFile << "\n\n" << endl;
                continue;
            }

            values = new TYPE[numElem];
            
            // read in elements from the file
            for(unsigned int i = 0; i < numElem; i++) {
                *inFile >> values[i];
            }

            // print the array;
            printArray(values, numElem, outFile);
            {
                // create a new task
                Mergesort<TYPE> mergeTask(values, 0, numElem-1, depth);
            }
            printArray(values, numElem, outFile);
            cout << endl;
            // delete allocated array
            delete values;
       }
       
       delete inFile; 
    } else {
        int* intArray;
        intArray = new int[size];
        for(int i = size; i > 0; i--) {
            intArray[i-1] = i;
        }
        
        {
            Mergesort<int> mergeTask(intArray, 0, size-1, depth);
        }

        delete intArray;
        
    }

    if(outFile != &cout) delete outFile;
}
