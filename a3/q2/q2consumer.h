#ifndef __CONSUMER_H__
#define __CONSUMER_H__
#include "q2boundedBuffer.h"

_Task Consumer {
    private:
	BoundedBuffer<int>& buffer; // the buffer to consume from
	const int delay; // used to randomly yield some number of times, up to delay-1
	const int sentinel; // a sentinel to determine when the consumer ends
	int& sum;

	void main();
    public:
	Consumer( BoundedBuffer<int> &buffer, const int Delay, const int Sentinel, int &sum );
};

#endif
