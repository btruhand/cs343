#include "q2producer.h"
#include "MPRNG.h"

extern MPRNG randomGen;

Producer::Producer(BoundedBuffer<int>& buffer, const int Produce, const int Delay) : buffer(buffer), produce(Produce), delay(Delay) {}

void Producer::main() {
    for(int i = 1; i <= produce; i++) {
	// yield some random amount of times, from 0 to delay-1
	yield(randomGen(0, delay-1));

	// insert to the buffer
	buffer.insert(i);
    }
}
