#include "MPRNG.h"
#include "q2boundedBuffer.h"
#include "q2producer.h"
#include "q2consumer.h"
#include <iostream>
#include <sstream>
using namespace std;

// global MPRNG to be used by both producer and consumer
MPRNG randomGen;

void usage(char** argv) {
    cerr << "Usage: " << argv[0]
         << "[ Cons (> 0) [ Prods (> 0) [ Produce (> 0) [ BufferSize (> 0) [ Delays (> 0) ] ] ] ] ]" << endl; 
    exit( EXIT_FAILURE );              // TERMINATE
} // usage

bool convert( int &val, char *buffer ) {      // convert C string to integer
    stringstream ss( buffer );         // connect stream and buffer
    ss >> dec >> val;                   // convert integer from buffer
    return ! ss.fail() &&               // conversion successful ?
           // characters after conversion all blank ?
           string( buffer ).find_first_not_of( " ", ss.tellg() ) == string::npos;
} // convert


void uMain::main() {
    int numCons = 5; // number of consumers
    int numProd = 3; // number of producers
    int produce = 10; // number of items to produce
    int bufferSize = 10; // the buffer size
    int delays = numCons+numProd; // determines the number of times a producer/consumer yields

    int totalSum = 0; // the total sum to be printed later
    int sentinel = -1; // the sentinel value to be checked by the consumers
    switch(argc) {
	case 6:
	    if(!convert(delays, argv[5]) || delays <= 0) usage(argv);
	case 5:
	    if(!convert(bufferSize, argv[4]) || bufferSize <= 0) usage(argv);
	case 4:
	    if(!convert(produce, argv[3]) || produce <= 0) usage(argv);
	case 3:
	    if(!convert(numProd, argv[2]) || numProd <= 0) usage(argv);
	case 2:
	    if(!convert(numCons, argv[1]) || numCons <= 0) usage(argv);
	case 1:
	    break;
    }

#ifdef __U_MULTI__
    uProcessor p[3] __attribute__((unused)); // create 3 kernel thread for a total of 4
#endif

    BoundedBuffer<int> buffer(bufferSize);

    // keep the producers and consumers in an array so we can deallocate them later
    Producer* producers[numProd];
    Consumer* consumers[numCons];

    int partialSum[numCons]; // partial sum from each consumer
    // create tasks on the heap
    for(int i = 0; i < numProd; i++) {
	producers[i] = new Producer(buffer, produce, delays);
	if(i < numCons) {
	    //initialize partialSum
	    partialSum[i] = 0;
	    consumers[i] = new Consumer(buffer, delays, sentinel, partialSum[i]);
	}

	if(i+1 == numProd) {
	    // if we're at the last iteration then create the rest of the consumers (if needed)
	    for(int j = i+1; j < numCons; j++) {
		partialSum[j] = 0;
		consumers[j] = new Consumer(buffer, delays, sentinel, partialSum[j]);
	    }
	}
    }

    // delete all the producers, and hence wait for them to finish
    for(int i = 0; i < numProd; i++) {
	delete producers[i];
    }

    // send the sentinel values to the consumers
    for(int i = 0; i < numCons; i++) {
	buffer.insert(sentinel);
    }

    // delete all the consumers, and hence wait for them to finish
    for(int i = 0; i < numCons; i++) {
	delete consumers[i];
	totalSum+= partialSum[i];
    }

    cout << "total: " << totalSum << endl;
}
