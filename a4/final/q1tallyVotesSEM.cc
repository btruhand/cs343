#include "q1tallyVotes.h"
#include "q1printer.h"

TallyVotes::TallyVotes(unsigned int group, Printer& printer) : groupSize(group), waitGroup(group), printer(printer), finalBallot(0) {
    // close this semaphore
    synchrosem.P();
}

bool TallyVotes::vote(unsigned int id, bool ballot) {
    // get lock
    bargesem.P();

    mutexsem.P();
    printer.print(id, Voter::Vote, ballot);

    // increase total ballot, false -> ballot = 0, true -> ballot = 1
    finalBallot+= ballot;
    waitGroup--; // number of group members to wait decreases

    if(waitGroup > 0) { // if not last voter in group
	printer.print(id, Voter::Block, groupSize - waitGroup); // print blocking
	mutexsem.V(); // release mutex sem
	synchrosem.P(bargesem); // let my group come in while sleeping on the synchro sem
	printer.print(id, Voter::Unblock, groupSize - (waitGroup+1)); // print unblocking, not including self
    } else printer.print(id, Voter::Complete); // signifies that voting process is complete

    bool returnBallot = (finalBallot <= groupSize/2) ? false:true; // ballot 0 or 1 depending on total ballots
    
    waitGroup++; // increase number of group members to wait
    
    if(waitGroup != groupSize) synchrosem.V(); // wake someone from my group if I'm not the last person, pass the baton
    else {
	finalBallot = 0; // reset
	mutexsem.V(); // release the lock so that others can finally get in
	bargesem.V(); // allow bargers to go in now
    }
    
    return returnBallot;
}
