#include "q2table.h"
#include "q2printer.h"

Table::Table(const unsigned int NoOfPhil, Printer& prt) : waitforforks(new uCondition[NoOfPhil]), forksUsed(new unsigned int[NoOfPhil]), numPhil(NoOfPhil), printer(prt) {
    for(unsigned int i = 0; i < numPhil; i++) {
	forksUsed[i] = numPhil;
    }
}


void Table::pickup(unsigned int id) {
    if(forksUsed[id] != numPhil || forksUsed[(id+1) % numPhil] != numPhil) {
	printer.print(id, Philosopher::Waiting);
	// forks not available to use yet
	waitforforks[id].wait();
    }
    
    forksUsed[id] = id;
    forksUsed[(id+1) % numPhil] = id;
    
    unsigned int myRightSide = (id == 0) ? numPhil-1:id-1; // the philosopher on my right
    unsigned int myLeftSide = (id+1) % numPhil; // the philosopher on my left
   
    // the philosopher to my right or left SHOULDN'T have their right
    if(forksUsed[myRightSide] == myRightSide || forksUsed[(myLeftSide+1) % numPhil] == myLeftSide) 
	uAbort("The one besides me got their forks too! How!?");

}

void Table::putdown(unsigned int id) {
    // not using forks anymore
    forksUsed[id] = numPhil;
    forksUsed[(id+1) % numPhil] = numPhil;

    unsigned int myRightSide = (id == 0) ? numPhil-1:id-1;
    unsigned int myLeftSide = (id+1) % numPhil;

    // the philosopher on my right want my forks and can get their right fork
    if(forksUsed[myRightSide] == numPhil && !waitforforks[myRightSide].empty()) {
	waitforforks[myRightSide].signal();

	// much like the semaphore implementation, 3 philosophers is a special case
	// for this method of implementation. Gives priority to philosophers on the right
	if(numPhil == 3) return;
    }

    // the philsopher on my left want my forks and can get their left fork
    if(forksUsed[(myLeftSide+1) % numPhil] == numPhil && !waitforforks[myLeftSide].empty()) {
	waitforforks[myLeftSide].signal();
    }
}

Table::~Table() {
    delete [] waitforforks;
    delete [] forksUsed;
}
