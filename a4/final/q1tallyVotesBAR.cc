#include "q1tallyVotes.h"
#include "q1printer.h"

TallyVotes::TallyVotes(unsigned int group, Printer& printer) : uBarrier(group), returnBallot(0), groupSize(group), waitGroup(group), printer(printer), finalBallot(0) {}

void TallyVotes::block(unsigned int id, bool ballot) {
    printer.print(id, Voter::Vote, ballot);
    finalBallot+= ballot;
    waitGroup--;
    if(waitGroup > 0) {
	printer.print(id, Voter::Block, groupSize - waitGroup);
    	uBarrier::block();
	printer.print(id, Voter::Unblock, groupSize - (waitGroup+1));
	waitGroup++;
    } else {
	printer.print(id, Voter::Complete);
	uBarrier::block();
    }
}

void TallyVotes::last() {
    returnBallot = (finalBallot <= groupSize/2) ? false:true; // ballot 0 or 1 depending on total ballots
    finalBallot = 0; // reset
    waitGroup++;
    uBarrier::last();
}

bool TallyVotes::vote(unsigned int id, bool ballot) {
    // block up to group-1 times, the group'th thread unblocks the rest
    block(id, ballot);
    return returnBallot;
}
