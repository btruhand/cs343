#include "MPRNG.h"
#include "q2table.h"
#include "q2printer.h"
#include <iostream>
using namespace std;

// global MPRNG
extern MPRNG randGen;

Philosopher::Philosopher(unsigned int id, unsigned int noodles, Table& table, Printer& prt) : id(id), numNoodles(noodles), table(table), printer(prt) {}

void Philosopher::main() {
    for(;;) {
	printer.print(id, Hungry); // I'm hungry

	yield(randGen(4)); // hungry time
	table.pickup(id); // pick up my forks
	unsigned int eatNumNoodles = randGen(1,5);
	if(eatNumNoodles > numNoodles) eatNumNoodles = numNoodles;
	numNoodles-= eatNumNoodles;
	printer.print(id, Eating, eatNumNoodles, numNoodles);
	yield(randGen(4)); // eating time
	table.putdown(id);

	if(numNoodles == 0) break;
	printer.print(id, Thinking);
	yield(randGen(19)); // thinking time
    }

    printer.print(id, Finished);
}
