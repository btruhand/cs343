#include "q2table.h"
#include "q2printer.h"

Table::Table(const unsigned int NoOfPhil, Printer& prt) : bargeFlag(false), bargeTicket(0), bargeServing(0), forksWant(new unsigned int[NoOfPhil]), forksUsed(new unsigned int[NoOfPhil]), numPhil(NoOfPhil), printer(prt){
    for(unsigned int i = 0; i < numPhil; i++) {
		forksUsed[i] = numPhil; // state that nobody is using the forks
		forksWant[i] = 0; // state that nobody wants the fork right now
    }
}

void Table::wait() {
    noforks.wait();                         // wait until signalled
    while ( rand() % 5 == 0 ) {             // multiple bargers allowed
        _Accept( pickup, putdown ) {        // accept barging callers
        } _Else {                           // do not wait if no callers
        } // _Accept
    } // while
}

void Table::signalAll() {                   // also useful
    while ( ! noforks.empty() ) noforks.signal(); // drain the condition
}

void Table::pickup(unsigned int id) {
    // are we barging or not? if yes then print an appropriate message and wait
    if(bargeFlag) {
		printer.print(id, Philosopher::Barging);
		unsigned int ticket = ++bargeTicket;
		while(ticket > bargeServing) { 
			// while I haven't been called to be served, I wait
			wait();
		}
	}

	// declare intent
	forksWant[id]++;
	forksWant[(id+1) % numPhil]++;

	while(forksUsed[id] != numPhil || forksUsed[(id+1) % numPhil] != numPhil) { // while I can't pick up my forks wait
		printer.print(id, Philosopher::Waiting);
		wait();
	}

    forksUsed[id] = forksUsed[(id+1) % numPhil] = id; // claim the forks to be mine
	
    unsigned int myRightSide = (id == 0) ? numPhil-1:id-1; // the philosopher on my right
    unsigned int myLeftSide = (id+1) % numPhil; // the philosopher on my left
   
    // the philosopher to my right or left SHOULDN'T have their their right and left forks respectively
    if(forksUsed[myRightSide] == myRightSide || forksUsed[(myLeftSide+1) % numPhil] == myLeftSide) 
		uAbort("The one besides me got their forks too! How!?");

	// retract intent
	forksWant[id]--;
	forksWant[(id+1) % numPhil]--;

}


void Table::putdown(unsigned int id) {
    // do not check for barging since calls to putdown never prevent a task from running
    
    // not holding the forks anymore
    forksUsed[id] = forksUsed[(id+1) % numPhil] = numPhil;

    unsigned int myRightSide = (id == 0) ? numPhil-1:id-1; // the philosopher on my right
    unsigned int myLeftSide = (id+1) % numPhil; // the philosopher on my left

    // check if somebody (whether my right or left philosophers) wants my forks and can actually get it
    if((forksUsed[myRightSide] == numPhil && forksWant[myRightSide] > 0 && forksWant[id] > 0) ||
			(forksUsed[(myLeftSide+1) % numPhil] == numPhil && forksWant[myLeftSide] > 0 && forksWant[(myLeftSide+1) % numPhil] > 0)) {
		bargeFlag = true; // change this later
		signalAll();
	} else if(bargeServing != bargeTicket) { 
		// there are bargers waiting, wake the ones that have waited for the longest time
		bargeServing++;
		signalAll();

		// bargeFlag not set here because it must be that bargeFLag = true if there are bargers, avoid redundant assignment
	} else if(noforks.empty()) bargeFlag = false; // no one to wake up (not even bargers), put down bargeFlag (change this later too)
}

Table::~Table() {
    delete [] forksUsed;
	delete [] forksWant;
}
