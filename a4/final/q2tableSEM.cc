#include "q2table.h"
#include "q2philosopher.h"
#include "q2printer.h"

Table::Table(const unsigned int NoOfPhil, Printer& prt) : forksWant(new unsigned int[NoOfPhil]), wakeupLeft(NoOfPhil),
    wakeupRight(NoOfPhil), wakeSignal(false), forksUsed(new unsigned int[NoOfPhil]), numPhil(NoOfPhil), printer(prt) {
    // nobody using forks yet nor nobody wants any fork
    for(unsigned int i = 0; i < numPhil; i++) {
	forksUsed[i] = numPhil; // signifies nobody is using it
	forksWant[i] = 0;
    }

    waitforforks.P(); // close the lock
}

void Table::pickup(unsigned int id) {
    getforksem.P(); // attempt to get the lock

    // signify I want forks, put intent
    forksWant[id]++;
    forksWant[(id+1) % numPhil]++;
   
    if(forksUsed[id] != numPhil || forksUsed[(id+1) % numPhil] != numPhil) {
	getforksem.V(); // release
	// possibly interrupted here and we might get our fork once we wake up again
	// the while loop checks this for us
	while(wakeupLeft != id && wakeupRight != id) {
	    // may be interrupted here
	    printer.print(id, Philosopher::Waiting);
	    // may be interrupted here
	    if(wakeupLeft != id && wakeupRight != id && wakeSignal) waitforforks.V();
	    // or here
	    
	    // it is fine though since we do daisy chaining, may take a while though
	    waitforforks.P(); // wait for my forks to be ready
	}
    }

    forksUsed[id] = forksUsed[(id+1) % numPhil] = id; // now I have mah forks muahahaha
    
    unsigned int myRightSide = (id == 0) ? numPhil-1:id-1; // the philosopher on my right
    unsigned int myLeftSide = (id+1) % numPhil; // the philosopher on my left
    
    // the philosopher to my right or left SHOULDN'T have their right
    if(forksUsed[myRightSide] == myRightSide || forksUsed[(myLeftSide+1) % numPhil] == myLeftSide) 
	uAbort("The one besides me got their forks too! How!?");

    // signify I don't want forks, retract intent
    forksWant[id]--;
    forksWant[(id+1) % numPhil]--;

    // if you were woken up check some things here, that is check if somebody else
    // should be woken up aside from you
    if(wakeupRight == id) {
	wakeupRight = numPhil;
	if(wakeupLeft == numPhil) wakeSignal = false;
	else waitforforks.V();
    } 
	
    if(wakeupLeft == id) { // if because of the case when there are only 2 philosophers
	wakeupLeft = numPhil;
	if(wakeupRight == numPhil) wakeSignal = false;
	else waitforforks.V();
    }
   
    // release those waiting to get inside only if no one else was woken up
    if(wakeupRight == numPhil && wakeupLeft == numPhil) getforksem.V();
}

void Table::putdown(unsigned int id) {
    getforksem.P(); // attempt to get the lock

    // tell the others I'm not using the forks anymore
    forksUsed[id] = numPhil;
    forksUsed[(id+1) % numPhil] = numPhil;

    unsigned int myRightSide = (id == 0) ? numPhil-1:id-1; // the philosopher on my right
    unsigned int myLeftSide = (id+1) % numPhil; // the philosopher on my left

    // checks if someone wants my fork and can get it i.e the philosopher on their left
    // and right aren't holding their forks
    if((forksUsed[myRightSide] == numPhil && forksWant[myRightSide] > 0 && forksWant[id] > 0) ||
	    (forksUsed[(myLeftSide+1) % numPhil] == numPhil && forksWant[myLeftSide] > 0 && forksWant[(myLeftSide+1) % numPhil] > 0)) {

	wakeSignal = true;
	if(forksUsed[myRightSide] == numPhil && forksWant[myRightSide] > 0 && forksWant[id] > 0) {
	    // the philosopher on my right is free to take his forks
	    wakeupRight = myRightSide;
	}

	if(forksUsed[(myLeftSide+1) % numPhil] == numPhil && forksWant[myLeftSide] > 0
		&& forksWant[(myLeftSide+1) % numPhil] > 0) {
	    // the philosopher on my left is free to take his forks
	    if(numPhil != 3 || wakeupRight == numPhil) {
		// this condition is needed for the case of 3 philosophers for this method of implementation
		// this gives priority to the philosopher on the right. This is okay since the pattern cycles
		wakeupLeft = myLeftSide;
	    }
	}

	// wake a philosopher up, pass the table lock to that philosopher
	waitforforks.V();
    } else getforksem.V(); // nobody can get/want to get my forks
}

Table::~Table() {
    delete [] forksWant;
    delete [] forksUsed;
}
