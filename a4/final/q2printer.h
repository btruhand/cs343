#ifndef __PRINTER_H__
#define __PRINTER_H__

#include "q2philosopher.h"

_Cormonitor Printer {
    private:
	unsigned int numPhils;
	unsigned int curphilID;
	unsigned int curphilBite;
	unsigned int curphilLeftNoodles;
	Philosopher::States curphilState;

	void assign(unsigned int id, Philosopher::States state);
	void main();
    public:
	Printer(unsigned int NoOfPhil);
	void print(unsigned int id, Philosopher::States state);
	void print(unsigned int id, Philosopher::States state, unsigned int bite, unsigned int noodles);
};

#endif
