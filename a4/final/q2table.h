#ifndef __TABLE_H__
#define __TABLE_H__

// Forward declaration of printer
_Cormonitor Printer;

#if defined( TABLETYPE_SEM )                // semaphore solution
// includes for this kind of table
#include <uSemaphore.h>

class Table {
    // private declarations for this kind of table
    private:
      // semaphore to pick up and and put down forks, lock the whole table
      uSemaphore getforksem;
      // semaphore to wait on when a philosopher can't pickup the forks
      uSemaphore waitforforks;
      unsigned int* forksWant; // array of intents i.e state philosophers want or do not want forks
      unsigned int wakeupLeft; // wake up the person left to the philosopher if possible
      unsigned int wakeupRight; // wake up the person right to the philosopher if possible
      bool wakeSignal; // indicate that one or more philosophers was woken up

#elif defined( TABLETYPE_INT )              // internal scheduling monitor solution
// includes for this kind of table
_Monitor Table {
    // private declarations for this kind of table
    private:
	uCondition* waitforforks;
#elif defined( TABLETYPE_INTB )             // internal scheduling monitor solution with barging
// includes for this kind of table
_Monitor Table {
    // private declarations for this kind of table
    private:
      uCondition noforks;                     // only one condition variable (you may change the variable name)
      void wait();                            // barging version of wait
      void signalAll();

      bool bargeFlag;
      unsigned int bargeTicket;
      unsigned int bargeServing;
      unsigned int* forksWant; // array of intents i.e state philosophers want or do not want forks
#else
    #error unsupported table
#endif
    // common declarations
      unsigned int* forksUsed; // whether or not the forks are being used
      const unsigned int numPhil;
      Printer& printer;
    public:                                   // common interface
      Table( const unsigned int NoOfPhil, Printer &prt );
      void pickup( unsigned int id );
      void putdown( unsigned int id );
      ~Table();
};

#endif
