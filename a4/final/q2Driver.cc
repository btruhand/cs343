#include "MPRNG.h"
#include "q2printer.h"
#include "q2table.h"
#include "q2philosopher.h"
#include <iostream>
#include <sstream>
#include <ctime>
using namespace std;

// global MPRNG to be used by voters
MPRNG randGen;

bool convert( int &val, char *buffer ) {		// convert C string to integer
    std::stringstream ss( buffer );			// connect stream and buffer
	    ss >> dec >> val;					// convert integer from buffer
		    return ! ss.fail() &&				// conversion successful ?
				// characters after conversion all blank ?
					string( buffer ).find_first_not_of( " ", ss.tellg() ) == string::npos;
} // convert

void usage(char* progname) {
	cerr << "Usage: " << progname
	     << " [ P (> 1)"
	     << " [ N (> 0)"
	     << " [ seed (> 0) ] ] ]" << endl;
	exit(EXIT_FAILURE);
}

void uMain::main() {
	int P = 5;
	int N = 30;
	int seed = time(NULL);
	switch(argc) {
		case 4:
		    if(!convert(seed, argv[3])) usage(argv[0]);;
		    if(seed <= 0) usage(argv[0]); // less than or equal to 0
		case 3:
		    if(!convert(N, argv[2])) usage(argv[0]);
		    if(N <= 0) usage(argv[0]); // less than or equal to 0 or even
		case 2:
		    if(!convert(P, argv[1])) usage(argv[0]);
		    if(P <= 1) usage(argv[0]); // less than or equal to 1 or not a multiple of G
		    break;
	}

	randGen.seed(seed); // set MPRNG seed

	// setup
	Printer printer(P);
	Table table(P, printer);
	
	Philosopher* philosophers[P];
	for(int i = 0; i < P; i++) {
	    philosophers[i] = new Philosopher(i, N, table, printer);
	}

	// cleanup
	for(int i = 0; i < P; i++) {
	    delete philosophers[i];
	}
}
