void Table::wait() {
    noforks.wait();                         // wait until signalled
    while ( rand() % 5 == 0 ) {             // multiple bargers allowed
        _Accept( pickup, putdown ) {        // accept barging callers
        } _Else {                           // do not wait if no callers
        } // _Accept
    } // while
}

void Table::signalAll() {                   // also useful
    while ( ! noforks.empty() ) noforks.signal(); // drain the condition
}

#if defined( TABLETYPE_SEM )                // semaphore solution
// includes for this kind of table
class Table {
    // private declarations for this kind of table
#elif defined( TABLETYPE_INT )              // internal scheduling monitor solution
// includes for this kind of table
_Monitor Table {
    // private declarations for this kind of table
#elif defined( TABLETYPE_INTB )             // internal scheduling monitor solution with barging
// includes for this kind of table
_Monitor Table {
    // private declarations for this kind of table
    uCondition noforks;                     // only one condition variable (you may change the variable name)
    void wait();                            // barging version of wait
#else
    #error unsupported table
#endif
    // common declarations
  public:                                   // common interface
    Table( const unsigned int NoOfPhil, Printer &prt );
    void pickup( unsigned int id );
    void putdown( unsigned int id );
};

_Task Philosopher {
  public:
    enum States { Thinking = 'T', Hungry = 'H', Eating ='E',
                   Waiting = 'W', Barging = 'B', Finished = 'F' };
    Philosopher( unsigned int id, unsigned int noodles, Table &table, Printer &prt );
};

_Monitor / _Cormonitor Printer {            // choose one of the two kinds of type constructor
  public:
    Printer( unsigned int NoOfPhil );
    void print( unsigned int id, Philosopher::States state );
    void print( unsigned int id, Philosopher::States state, unsigned int bite, unsigned int noodles );
};

_Monitor MPRNG {
  public:
    MPRNG( unsigned int seed = 1009 ) { srand( seed ); }    // set seed
    void seed( unsigned int seed ) { srand( seed ); }       // set seed
    unsigned int operator()() { return rand(); }            // [0,UINT_MAX]
    unsigned int operator()( unsigned int u ) { return operator()() % (u + 1); } // [0,u]
    unsigned int operator()( unsigned int l, unsigned int u ) { return operator()( u - l ) + l; } // [l,u]
};
