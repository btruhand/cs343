#ifndef __PHILOSOPHER_H__
#define __PHILOSOPHER_H__

// Forward declaration of Table and Printer
_Cormonitor Printer;
#if defined(TABLETYPE_SEM)
class Table;
#else
_Monitor Table;
#endif

_Task Philosopher {
    private:
	unsigned int id;
	unsigned int numNoodles;
	Table& table;
	Printer& printer;
	void main();
    public:
	enum States { Thinking = 'T', Hungry = 'H', Eating = 'E',
	    	      Waiting = 'W', Barging = 'B', Finished = 'F' };
	Philosopher(unsigned int id, unsigned int noodles, Table& table, Printer& prt);
};

#endif
