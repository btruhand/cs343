Here is a list of tests that have been conducted on the programs:

- Command arguments
- Printing format
- Concurrency testing
- My Left and Right Philosopher testing
- The three philosophers testing

Command Arguments
=================

This test primarily consists of checking whether or not the program handles cases of incrroect values of the command
arguments or when malformed input is given. Since the test is only checking the driver, we will only test using one
implementation. Here are the test runs:

     2	btruhand@ubuntu1204-002:~/cs343/a4/philosopher$ ./philSEM 0
     3	Usage: ./philSEM [ P (> 1) [ N (> 0) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-002:~/cs343/a4/philosopher$ ./philSEM 1
     5	Usage: ./philSEM [ P (> 1) [ N (> 0) [ seed (> 0) ] ] ]
	 6	btruhand@ubuntu1204-002:~/cs343/a4/philosopher$ ./philSEM -1
	 7 	Usage: ./philSEM [ P (> 1) [ N (> 0) [ seed (> 0) ] ] ]
     7	btruhand@ubuntu1204-002:~/cs343/a4/philosopher$ exit
     8	exit

We see here that the program correctly behaves when given a value of P, the number of philosophers, that is below or
equal to 1 by printing a usage message.

     2	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ ./philSEM 2 0
     3	Usage: ./philSEM [ P (> 1) [ N (> 0) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ ./philSEM 2 -1
     5	Usage: ./philSEM [ P (> 1) [ N (> 0) [ seed (> 0) ] ] ]
     6	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ exit
     7	exit

In this case we see that the program behaves correctly when given a value of N, the number of noodles per plate, that is
below 1 by printing a usage message. We know that the program is reacting towards the incorrect value of N since the
value of P is correct, and the seed is taken to be random which would always lead to a correct value for the seed.
Hence it must be due to the value of N that the usage message is printed.

     2	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ ./philSEM 2 1 -129
     3	Usage: ./philSEM [ P (> 1) [ N (> 0) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ ./philSEM 2 1 0
     5	Usage: ./philSEM [ P (> 1) [ N (> 0) [ seed (> 0) ] ] ]
     6	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ exit
     7	exit

Similarly like cases before here we are checking whether an incorrect value of seed is treated correctly and it clearly
is.

     2	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ ./philSEM 1029fa.29
     3	Usage: ./philSEM [ P (> 1) [ N (> 0) [ seed (> 0) ] ] ]
     4	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ ./philSEM 1020 1298das.w
     5	Usage: ./philSEM [ P (> 1) [ N (> 0) [ seed (> 0) ] ] ]
     6	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ ./philSEM 1020 1298 9138asss...
     7	Usage: ./philSEM [ P (> 1) [ N (> 0) [ seed (> 0) ] ] ]
     8	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ exit
     9	exit
    
Here we are testing whether malformed input for all command arguments are treated properly or not. By malformed input
I mean non-integers and the usage message each time indicates that malformed input are treated appropriately.


     2	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ ./philSEM > out
     3	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ exit
     4	exit
     
	 1	Phil0	Phil1	Phil2	Phil3	Phil4
     2	******	******	******	******	******
     3	H	H
     4		E1,29
     5		T	H	H	H
     6	E5,25		E5,25
     ... (further output not shown)

In this test we see whether the program uses the appropriate default values for P and N or not when they are not
specified. Clearly the number of philosophers is correct which is five. The number of noodles by default should be 30
and we see this is indeed the case. This is because we notice that when Phil1 eats the first time, the remaining
number of noodles is 29 and he is eating 1 noodle, which would imply that the initial number of noodles is 30. The
same can be derived by observing when Phil0 and Phil2 first eats. Hence the program has the correct default
values for P and N

Printing format
===============

For brevity the tests will only be done with small inputs. Though it should easily generalize to bigger inputs. In this
test we would like to see whether the programs have the correct print format. In particular we would like to see the
following:

- A H is printed before a philosopher tries to grab the forks
- When a philosopher waits Wr,l is printed where r is the right fork and l is the left fork of the philosopher
- Ea,b should be printed after everytime a philosopher manages to grab the forks where a is the number of noodles being
eaten by the philosopher and b is the number of noodles left
- T should be printed every time a philosopher finishes eating
- When a philosopher finishes F is printed on its column and ... on the rest
- Lastly the philosopher always eats the same number of noodles as the number of noodles left on the plate before
a print of F

     2	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ ./philSEM 3 6 > out
     3	btruhand@ubuntu1204-006:~/cs343/a4/philosopher$ exit
     4	exit
     
	 1	Phil0	Phil1	Phil2
     2	******	******	******
     3	H	H	H
     4	W0,1	E3,3
     5		T
     6	E2,4	H	W2,0
     7	T	W1,2	E4,2
     8		E2,1	T
     9		T	H
    10			E1,1
    11	H		T
    12	E2,2		H
    13	T	H
    14		E1,0	W2,0
    15	...	F	...
    16			E1,0
    17	...	...	F
    18	H
    19	E2,0
    20	F	...	...
    21	***********************
    22	Philosophers terminated

We can see clearly that the semaphore implementation follows all the required guidelines that were mentioned above. We
conclude then that the semaphore implementation has the correct printing format.


     1	Script started on Wed 05 Nov 2014 07:35:53 PM EST
     2	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 3 6 >out
     3	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ exit
     4	exit
     
	 1	Phil0	Phil1	Phil2
     2	******	******	******
     3	H	H	H
     4	E5,1
     5	T	W1,2	E4,2
     6		E5,1	T
     7	H	T	H
     8			E2,0
     9	...	...	F
    10	E1,0
    11	F	...	...
    12		H
    13		E1,0
    14	...	F	...
    15	***********************
    16	Philosophers terminated

We also do not see anything wrong with the print format for the monitor implementation.

Another constraint given to the barging monitor implementation is:
- A B for some philosopher should be printed whenever a philosopher is woken and the philosopher barges into the
pickup function since the philosopher is barging.

     2	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB> out
     3	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ exit
     4	exit
     
	 1	Phil0	Phil1	Phil2
     2	******	******	******
     3	H	H	H
     4	W0,1	E1,5
     5	E2,4	T
     6		H	B
     7	T		W2,0
     8			E5,1
     9		B	T
    10		E2,3
    11		T	H
    12			B
    13			E1,0
    14	...	...	F
    15	H
    16	B
    17	E3,1
    18	T	H
    19		B
    20		E3,0
    21	...	F	...
    22	H
    23	B
    24	E1,0
    25	F	...	...
    26	***********************
    27	Philosophers terminated

Indeed for the barging monitor implementation there are Bs that are printed. However it might not be clear as to why.
The reasonis because in the way I implemented the barging monitor, I made barging philosophers go to sleep. Of course
they too need to be woken up then. Thus it is likely that these Bs are caused by a philosopher that is trying to barge
while a philosopher that barged is woken up.

    ... (output not shown) 
    13		T	W2,0
    14	B		E2,6
    ...

The above is a snippet of output from running the barging monitor implementation as follows: ./philINTB 3 9 > out.
The above shows the case when a philosopher is likely to be barging on a philosopher that was waiting for his
forks thus the B is printed.

Thus in either case we see that a B is appropriately printed and hence we conclude that the barging monitor
implementaton has the correct print format since t satisfies all our guidelines.

Concurrency Testing
===================

We want to see whether or not the programs actually allow concurrent progress. We shall see this by observing that
two different philosophers which are distinctly apart i.e do not share forks with one another can run 'together'
or seemingly so. The idea is that if the programs do allow concurrent progress then philosophers that do not conflict
with each other should be able to run together. For brevity's sake I will only give snippets of the output:

     2	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 5 14 > out
     3	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ exit
     4	exit

     1	Phil0	Phil1	Phil2	Phil3	Phil4
     2	******	******	******	******	******
	... (output not shown)
    13	H	T	H		E4,7
    14	E3,3	H	E3,5		T
    15	T		T	H
	... (output not shown)
	26		E1,7	H		H
    27		T	E2,3	H	E3,0
    28			T	W3,4
	... (output not shown)

On line 14 we see that Phil0 is able to seemingly eat at the same time as Phil2, which certainly should be allowed
since they don't share forks together. The same case can be observed on line 27 
     
     2	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 7 23 > out
     3	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ exit
     4	exit
     
	 1	Phil0	Phil1	Phil2	Phil3	Phil4	Phil5	Phil6
     2	******	******	******	******	******	******	******
     ... (output not shown)
	 5	W0,1	E3,20	T	E1,22		W5,6	E3,20
     6				T	E3,20
     7		T			T	E4,19	T
     ... (output not shown)
	10	H	E3,17	W2,3				E2,18
    11		T	E3,19	H	E5,15		T
    12	E2,18		T	W3,4	T	H
     ... (output not shown)
	20	W0,1	E3,14		E5,13	W4,5	H
     ... (output not shown)

We can see here on various lines (5, 10, 11, 20) that multiple philosophers which do not share forks are able to run
seemingly simultaneously together.


     2	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 5 10 > out
     3	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ exit
     4	exit
     
	 1	Phil0	Phil1	Phil2	Phil3	Phil4
     2	******	******	******	******	******
     3	H	H	H	H	H
     4	E3,7		E1,9
     5	T	W1,2	T	W3,4	E1,9
     ...
	23		E3,4			E1,4
    24		T	H	H	T
    25				B
     ...

We see again that even in the barging monitor implementation that we can see concurrent progress happening. Hence we
will conclude that for all implementations they all enable concurrent progress.

My Left and Right Philosophers Testing
======================================

If a philosopher, say A, picks up his forks it is definitely because his left and right forks are vacant. 
Thus a philosopher should never pick up a fork when either:
- The philosopher to his left has his forks since then that philosopher's right fork is the left fork of A
- The philosopher to his right has his forks since then that philosopher's left fork is the right fork of A

Of course once A has his forks it is also imperative that the philosophers to his left and right cannot grab his
forks due to the same conceptual reason stated above.

In all of the implementations I have this piece of code that checks for this condition to be fulfilled:

	if(forksUsed[myRightSide] == myRightSide || forksUsed[(myLeftSide+1) % numPhil] == myLeftSide) 
		uAbort("The one besides me got their forks too! How!?");

The condition statement checks that the philosopher on the right of some philosopher does not have his right fork
and the pholosopher to the left of the same philosopher does not have his left fork. The rationale is that:
- If the philosopher to the right has his right fork then surely he will have his left fork
- If the philosopher to the left has his left fork then surely he will have his right fork

If the condition is violated, we abort. Thus we shall test this by having various inputs to all implementation
and see if any of them causes a violation.

     2	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 16 78 > out
     3	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 33 44 > out
     4	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 19 20 > out
     5	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 19 4 > out
     6	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 19 4 > out
     7	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 56 120 > out
     8	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 33 77 > out
     9	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 90 1 > out
    10	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 90 7 > out
    11	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 100 3 > out
    12	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 58 65 > out
    13	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 3 3 > out
    14	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 89 77 > out
    15	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ exit
    16	exit

We have run several tests of all of the implementations with various inout. Note that in none of them I specified a
seed because I thought that randomness would only help us in trying to capture a possible violation of the condition.
It would seem that based on the above tests that the condition is not violated. I have also done more tests though
not shown here for brevity's sake and in none of them have the condition been violated. We conclude then that the
implementations all ensure that the condition is never violated.

The Three Philosophers Testing
==============================

A unique scenario for this problem is when there are only three philosophers. Here is a layout of what the table may
look like (variations only involve where the philosophers sit):

				Phil0
			  -		  -
			Phil1	Phil2
				  -

A '-' represents a fork. Now notice that whenever ANY philosopher on the table grabs a fork it must be that the
philosophers to the left and right of that philosopher cannot pick up their forks, or at least they should not be able
to, or else it'll be a violation of the above condition. The interesting part is that when the philosopher puts down
his fork, these forks would be available to the other two philosophers but only one of them can get them because
these two philosophers would surely share a fork. Thus only one of them can actually pick it up. We want to see
whether or not the implementation behaves correctly in this scenario.


     2	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 3 > out
     3	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 3 > out
     4	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 3 1028 > out
     5	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 3 1028 > out
     6	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 3 1028 348 > out
     7	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 3 1028 348 > out
     8	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 3 50 348128 > out
     9	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 3 50 348128 > out
    10	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 3 50 348128 > out
    11	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 3 50 348128 > out
    12	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 3 348128 > out
    13	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 3 123 19273 > out
    14	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 3 123 19273 > out
    15	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 3 123 > out
    16	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINT 3 55 > out
    17	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 3 55 > out
    18	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 3 1000 > out
    19	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 3 234 > out
    20	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 3 212873 > out
    21	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 3 234 12 > out
    22	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 3 999 12 > out
    23	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philINTB 3 9299 > out
    24	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ ./philSEM 3 9299 > out
    25	btruhand@ubuntu1204-004:~/cs343/a4/philosopher$ exit
    26	exit

From the tests above it would seem that the implementations correctly handle this particular case since none
of them aborted. We conclude then that all implementations handle this case correctly.
