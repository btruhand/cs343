#include "q2printer.h"
#include <iostream>
using namespace std;

Printer::Printer(unsigned int NoOfPhil) : numPhils(NoOfPhil) { resume(); } // start printer

void Printer::main() {
    unsigned int maxID = 0;
    char philStates[numPhils]; // states
    unsigned int philBites[numPhils]; // bites
    unsigned int philLeftNoodles[numPhils]; // their left noodles
    unsigned int donePhil = 0;

    // initialize the philStates and print the layout
    for(unsigned int i = 0; i < numPhils; i++) {
	philStates[i] = '\0';
	cout << "Phil" << i;
	if(i != numPhils-1) cout << '\t';
    }

    cout << endl;

    for(unsigned int i = 0; i < numPhils; i++) {
	cout << "******";
	if(i != numPhils-1) cout << '\t';
    }

    cout << endl;

    suspend(); // wait for printing to start

printformat:
    for(;;) {
	if(philStates[curphilID] != '\0' || curphilState == Philosopher::Finished) {
	    for(unsigned int i = 0; i <= maxID; i++) {
		if(philStates[i] != '\0') {
		    cout << philStates[i];
		    if(philStates[i] == '.') cout << ".."; // print "..." with the above
		    else if(philStates[i] == Philosopher::Waiting) cout << i << "," << (i+1) % numPhils;
		    else if(philStates[i] == Philosopher::Eating) cout << philBites[i] << ',' << philLeftNoodles[i];

		    philStates[i] = '\0'; // flush
		}

		if(curphilState == Philosopher::Finished) philStates[i] = '.';
		if(i != maxID) cout << '\t';
	    }
	    cout << endl;
	    maxID = 0;
	    if(curphilState == Philosopher::Finished) {
		for(unsigned int i = 0; i < numPhils; i++) {
		    if(i != curphilID) philStates[i] = '.';
		    else philStates[i] = curphilState;
		}
		if(++donePhil == numPhils) break printformat; // break if all philosophers are done
		maxID = numPhils-1;
	    }
	}

	// set new maxID
	if(curphilID > maxID) maxID = curphilID;
	philStates[curphilID] = curphilState;
	if(curphilState == Philosopher::Eating) { // store information if eating
	    philBites[curphilID] = curphilBite;
	    philLeftNoodles[curphilID] = curphilLeftNoodles;
	}

	suspend();
    }

    // if we get here then all philosophers are finished, now we have to print finish for the last philosopher
    for(unsigned int i = 0; i < numPhils; i++) {
	if(i != curphilID) cout << "...";
	else cout << "F";

	if(i != numPhils-1) cout << '\t';
    }

    cout << "\n***********************\nPhilosophers terminated" << endl;

    // don't return to uMain, printer's cocaller
    suspend();
}


void Printer::assign(unsigned int id, Philosopher::States state) {
    curphilID = id;
    curphilState = state;
}

void Printer::print(unsigned int id, Philosopher::States state) {
    assign(id, state);
    resume();
}

void Printer::print(unsigned int id, Philosopher::States state, unsigned int bite, unsigned int noodles) {
    assign(id, state);
    curphilBite = bite;
    curphilLeftNoodles = noodles;
    resume();
}
