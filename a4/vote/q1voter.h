#ifndef __VOTER_H__
#define __VOTER_H__

#include "q1tallyVotes.h"

// Forward declaration
_Cormonitor Printer;

_Task Voter {
	private:
		unsigned int id;
		TallyVotes& tallier;
		Printer& printer;

		void main();
	public:
		enum States { Start = 'S', Vote = 'V', Block = 'B', Unblock = 'U',
				   Complete = 'C', Finished = 'F' };
		Voter( unsigned int id, TallyVotes &voteTallier, Printer &printer );
};

#endif
