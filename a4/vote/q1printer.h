#ifndef __PRINTER_H__
#define __PRINTER_H__

#include "q1voter.h"

_Cormonitor Printer {     // chose one of the two kinds of type constructor
    private:
	unsigned int numVoters; // number of voters
	unsigned int curvoterID; // current voter's (the one that wants to print) ID
	Voter::States curvoterState; // current voter's state
	unsigned int miscData; // miscellaneous data to be printed
	void assign(unsigned int id, Voter::States state); // used to assign values
	void main();
    public:
	Printer( unsigned int voters );
	void print( unsigned int id, Voter::States state );
	void print( unsigned int id, Voter::States state, bool vote );
	void print( unsigned int id, Voter::States state, unsigned int numBlocked );
};

#endif
