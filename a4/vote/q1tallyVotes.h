#ifndef __TALLYVOTES_H__
#define __TALLYVOTES_H__

#if defined(IMPLTYPE_SEM)
#include <uSemaphore.h>
#elif defined(IMPLTYPE_BAR)
#include <uBarrier.h>
#endif

// Forward declaration
_Cormonitor Printer;

#if defined( IMPLTYPE_MC )           // mutex/condition solution
// includes for this kind of vote-tallier
class TallyVotes {
    // private declarations for this kind of vote-tallier
    private:
	uOwnerLock ownerlk;
	uCondLock condlk;
	uCondLock bargelk;
	bool bargeFlag;
#elif defined( IMPLTYPE_SEM )        // semaphore solution
// includes for this kind of vote-tallier
class TallyVotes {
    private:
	uSemaphore mutexsem;
	uSemaphore synchrosem;
	uSemaphore bargesem;
    // private declarations for this kind of vote-tallier
#elif defined( IMPLTYPE_BAR )        // barrier solution
// includes for this kind of vote-tallier
_Cormonitor TallyVotes : public uBarrier {
    // private declarations for this kind of vote-tallier
    private:
	void block(unsigned int id, bool ballot);
	void last();
	bool returnBallot;
#else
    #error unsupported voter type
#endif
    // common declarations
	unsigned int groupSize; // size of a group
	unsigned int waitGroup; // number of group members left to enter vote()
	Printer& printer;
	unsigned int finalBallot;
    public:                            // common interface
    	TallyVotes( unsigned int group, Printer &printer );
	bool vote( unsigned int id, bool ballot );
};

#endif
