#include "q1tallyVotes.h"
#include "q1printer.h"
#include "q1voter.h"
#include <iostream>
using namespace std;

TallyVotes::TallyVotes(unsigned int group, Printer& printer) : bargeFlag(false), groupSize(group), waitGroup(group), printer(printer), finalBallot(0) {}

bool TallyVotes::vote(unsigned int id, bool ballot) {
	// get lock
	ownerlk.acquire();

	if(bargeFlag) { // barge flag is up i.e somebody was woken up
	    bargelk.wait(ownerlk);
	}
	
	printer.print(id, Voter::Vote, ballot);
	// increase total ballot, false -> ballot = 0, true -> ballot = 1
	finalBallot+= ballot;
	waitGroup--; // number of group members to wait decreases
	
	if(waitGroup > 0) { // if not last voter in group
	    printer.print(id, Voter::Block, groupSize - waitGroup); // print blocking
	    if(bargelk.empty()) bargeFlag = false; // none of my group members are barged
	    else bargelk.signal(); // signal a barger to wake up, barger is part of my group
	    condlk.wait(ownerlk); // wait
	    printer.print(id, Voter::Unblock, groupSize - (waitGroup+1)); // print unblocking, not including self
	} else {
	    bargeFlag = true;
	    printer.print(id, Voter::Complete);
	}

	bool returnBallot = (finalBallot <= groupSize/2) ? false:true; // ballot 0 or 1 depending on total ballots
	
	waitGroup++; // increase number of group members to wait

	condlk.signal(); // signal my group mate
	if(waitGroup == groupSize) {
	    finalBallot = 0; // reset to 0 for next group
	    if(bargelk.empty()) bargeFlag = false; // nobody to signal, no barging possible then
	    else bargelk.signal(); // wake somebody up from the barger if you're last voter to get out
	}

	ownerlk.release();
	return returnBallot;
}
