#include <iostream>
#include <stdlib.h>
#include <sstream>
using namespace std;

int main(int argc, char** argv) {
    
    stringstream iss(argv[1]);
    int type;
    iss >> type;
    if(type == 1) {
	char bytes[] = {
		0x23, '\n',					// valid
		0x23, 0x23, '\n',				// valid, extra byte
		0xd7, 0x90, '\n',				// valid
		0xd7, '\n',					// invalid, missing a byte
		0xc2, 0xa3, '\n',				// valid
		'\n',						// empty line
		0xb0, '\n',					// invalid, value too large
		0xe0, 0xe3, '\n',				// invalid, second byte wrong, missing a byte
		0xe9, 0x80, 0x80, '\n',				// valid
		0xe9, 0x80, 0x80, 0xff, 0xf8, '\n',		// valid, extra 2 bytes
		0xe0, 0x93, 0x90, '\n',				// invalid, value encoded in wrong range
		0xff, 0x9A, 0x84, '\n',				// invalid, first byte, extra 2 bytes 
		0xf0, 0x90, 0x89, '\n',				// invalid, missing byte
		0xf0, 0x90, 0x89, 0x80, '\n',			// valid
		0x01, '\n',					// valid
		0xdc, 0x81, '\n',				// valid
	    };
	    for ( int i = 0; i < sizeof( bytes ); i += 1 ) {
		cout << bytes[i];
	    }
     } else if(type == 2) {
	    char bytes[] = {
		0xc2, '\n',
		0xe0, '\n',
		0xe0, 0xbc, '\n',
		0xf2, '\n',
		0xf2, 0x90, '\n',
		0xf2, 0x90, 0x80, '\n',
	    };
	    for ( int i = 0; i < sizeof( bytes ); i += 1 ) {
		cout << bytes[i];
	    }
      } else if(type == 3) {
	  char bytes[] = {
	      0x0, '\n',
	      0x7F, '\n',
	      0xc2, 0x80, '\n',
	      0xdf, 0xbf, '\n',
	      0xe0, 0xa0, 0x80, '\n',
	      0xef, 0xbf, 0xbf, '\n',
	      0xf0, 0x90, 0x80, 0x80, '\n',
	      0xf4, 0x8f, 0xbf, 0xbf, '\n',
	  };
	  for ( int i = 0; i < sizeof( bytes ); i += 1 ) {
		cout << bytes[i];
	    }
      } else if(type == 4) {
	  char bytes[] = {
	      0xf8, '\n',
	      0x89, '\n',
	      0x90, '\n',
	      0xa0, '\n',
	      0xb0, '\n',
	      0xc2, 0x00, '\n',
	      0xe0, 0x00, 0x70, '\n',
	      0xe0, 0xb0, 0x70, '\n',
	      0xf0, 0x10, 0x80, 0x80, '\n',
	      0xf0, 0x90, 0x12, 0x90, '\n',
	      0xf0, 0x90, 0x8f, 0x10, '\n',
	  };
	for ( int i = 0; i < sizeof( bytes ); i += 1 ) {
		cout << bytes[i];
	    }
      } else if(type == 5) {
	  char bytes[] = {
	      0xc0, 0x8f, '\n',
	      0xe0, 0x90, 0x80, '\n',
	      0xf0, 0x81, 0x80, 0x80, '\n',
	      0xf5, 0x8f, 0xbf, 0xbf, '\n',
	  };
	for ( int i = 0; i < sizeof( bytes ); i += 1 ) {
		cout << bytes[i];
	    }
      }

}
