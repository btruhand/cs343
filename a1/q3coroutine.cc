#include "q3coroutine.h"

// macros for readable code, has to do with control flow
#define WORKING 0
#define MATCH 1
#define ERROR 2


Utf8::Utf8(int& state, unsigned int& unicode) : state(state), unicode(unicode), firstutf8(0), secondutf8(6), 
    thirdutf8(14), fourthutf8(30), datautf8(2) {state = WORKING; unicode = 0;}

void Utf8::main() {
    int iter;  // the number of iterations needed for each utf8 char type
    unsigned int MainUnicode;  // the unicode equivalent of the utf8 char
    try {
	// we shall check which utf8 type it is
	if(utf8.t1.ck == firstutf8) {
	    iter = 0;
	    MainUnicode = utf8.t1.dt;
	}
    	else if(utf8.t2.ck == secondutf8) {
	    iter = 1;
	    MainUnicode = utf8.t2.dt;
	}
    	else if(utf8.t3.ck == thirdutf8) {
	    iter = 2;
	    MainUnicode = utf8.t3.dt;
	}
    	else if(utf8.t4.ck == fourthutf8) {
	    iter = 3;
	    MainUnicode = utf8.t4.dt;
	} else {
	    // none of the utf8 type, so immediately throw error
	    throw Error();
    	}

	for(int i = 0; i < iter; i++) {
	    // iterate through the rest of the utf8
	    suspend();
	    if(utf8.ch == '\n') {
		// incomplete utf8 byte sequence
		throw Error();
	    }
	    if(utf8.dt.ck == datautf8) { // check if it's prefixed by 10 (binary)
		MainUnicode = (MainUnicode << 6) + utf8.dt.dt; // shift left by 6 then add the data
	    } else { // Error
		throw Error();
	    }
	}
	
	// check if the unicode is actually correct for the corresponding utf8 type
	if(iter == 0) {
	    if(MainUnicode >= 0x0 && MainUnicode <= 0x7F) {
		throw Match(MainUnicode);
	    }
	} else if(iter == 1) {
	    if(MainUnicode >= 0x80 && MainUnicode <= 0x7FF) {
		throw Match(MainUnicode);
	    }
	} else if(iter == 2) {
	    if(MainUnicode >= 0x800 && MainUnicode <= 0xFFFF) {
		throw Match(MainUnicode);
	    }
	} else {
	    if(MainUnicode >= 0x10000 && MainUnicode <= 0x10FFFF) {
		throw Match(MainUnicode);
	    }
	}

	// if we ever get here, then it means the above check fails so we throw an Error
	throw Error();
    } catch(Match& M) {
	unicode+= M.unicode;
	state = MATCH;
    } catch(Error) {
	state = ERROR;
    }
}
