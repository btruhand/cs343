// macros for readable code, has to do with control flow
#define WORKING 0
#define MATCH 1
#define ERROR 2


_Coroutine Utf8 {
  public:
    struct Match {
        unsigned int unicode;
        Match( unsigned int unicode ) : unicode( unicode ) {}
    };
    struct Error {};
  private:
    union UTF8 {
        unsigned char ch;               // character passed by cocaller
#if defined( _BIG_ENDIAN ) || BYTE_ORDER == BIG_ENDIAN    // BIG ENDIAN architecture
        struct {                        // types for 1st utf-8 byte
            unsigned char ck : 1;       // check
            unsigned char dt : 7;       // data
        } t1;
        struct {
            unsigned char ck : 3;       // check
            unsigned char dt : 5;       // data
        } t2;
        struct {
	    unsigned char ck : 4;
	    unsigned char dt : 4;
        } t3;
        struct {
	    unsigned char ck : 5;
	    unsigned char dt : 3;
        }t4;
        struct {                        // type for extra utf-8 bytes
            unsigned char ck : 2;
	    unsigned char dt : 6;
        } dt;
#else                                   // LITTLE ENDIAN architecture
        struct {                        // types for 1st utf-8 byte
            unsigned char dt : 7;       // data
            unsigned char ck : 1;       // check
        } t1;
        struct {
            unsigned char dt : 5;       // data
            unsigned char ck : 3;       // check
        } t2;
        struct {
	    unsigned char dt : 4;
	    unsigned char ck : 4;
        } t3;
        struct {
            unsigned char dt : 3;
	    unsigned char ck : 5;
        } t4;
        struct {                        // type for extra utf-8 bytes
            unsigned char dt : 6;
	    unsigned char ck : 2;
        } dt;
#endif
    } utf8;

    int& state;				// this state variable is needed sadly with what we've been taught so far
    unsigned int& unicode;		// valid UTF8 unicode value will be given through this variable

    // utf8 types based on the prefixes on their first byte
    int firstutf8;
    int secondutf8;
    int thirdutf8;
    int fourthutf8;
    // prefix for the rest of the utf8 byte sequence i.e 10(binary), 2(decimal)
    int datautf8;

    void main();
  public:
    // YOU MAY ADD CONSTRUCTOR/DESTRUCTOR IF NEEDED
    void next( unsigned char c ) {
        utf8.ch = c;                    // insert character into union for analysis
        resume();
        // if necessary throw Match or Error exception
    }

    // CONSTRUCTOR
    Utf8(int& state, unsigned int& unicode);
};
