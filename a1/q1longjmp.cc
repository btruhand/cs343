#include <iostream>
using namespace std;
#include <cstdlib>                                // exit, atoi
#include <setjmp.h>

struct T {
        ~T() { cout << "~T" << endl; }
};

jmp_buf jmp_env;				  //jmp_buf variable used to jump between routines 
unsigned int hc, gc, fc, kc;

void f( volatile int i ) {                        // volatile, prevent dead-code optimizations
    T t;
    cout << "f enter" << endl;
    if ( i == 3 ) longjmp(jmp_env,1);		  // jump back to where setjmp was previously called, return value of setjmp is 1
    if ( i != 0 ) f( i - 1 );
    cout << "f exit" << endl;
    kc += 1;                                      // prevent tail recursion optimization
}

void g( volatile int i ) {
    cout << "g enter" << endl;
    if ( i % 2 == 0 ) f( fc );
    if ( i != 0 ) g( i - 1 );
    cout << "g exit" << endl;
    kc += 1;
}

void h( volatile int i ) {
    cout << "h enter" << endl;
    if ( i % 3 == 0 ) {
	if(!setjmp(jmp_env)) { 
	    // if setjmp returns 0 i.e before f() is called, enter
	    f( fc );
	} else {
	    // a longjmp occurred and we got here
	    cout << "handler 1" << endl;
	    if(setjmp(jmp_env)) { 
		// setjmp would return 1 at first, turn it back so that it returns 0
		// needed so that "handler 2" can be printed out
		longjmp(jmp_env,0);
	    } else {
		if(!setjmp(jmp_env)) {
		    g( gc );
		} else {
		    cout << "handler 2" << endl;
		}
	    }
	}
    }
    if ( i != 0 ) h( i - 1 );
    cout << "h exit" << endl;
    kc += 1;
} 

int main( int argc, char *argv[] ) {
        switch ( argc ) {
	    case 4: fc = atoi( argv[3] );               // f recursion depth
	    case 3: gc = atoi( argv[2] );               // g recursion depth
	    case 2: hc = atoi( argv[1] ); break;        // h recursion depth
	    default: cerr << "Usage: " << argv[0] << " hc gc fc" << endl; exit( EXIT_FAILURE );
	}
	if ( hc < 0 || gc < 0 || fc < 0 ) {
	    cerr << "Input less than 0" << endl;
	    exit( EXIT_FAILURE );
	}
	h( hc );
}
