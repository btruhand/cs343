#include <iostream>
#include <stdlib.h>
using namespace std;

class H {                                               // uC++ exception type
  public:
    int &i;                                             // pointer to fixup variable at raise
    int initTimes; 					// initial value of times i.e the total depth of the recursion 
    H( int &i ) : i(i), initTimes(i) {}
    void operator()(void) {
	if(initTimes == i) cout << "root " << i << endl;
	else cout << "f handler " << i << endl;
    }
};

void f(int &i, H& h) {
    cout << "f " << i << endl;
    if ( rand() % 5 == 0 ) h();               		// require correction ?
    
    i-=1;
    if ( 0 < i ) f(i,h);                             	// recursion
}

int main(int argc, char* argv[]) {
    int times = 25, seed = getpid();
    switch ( argc ) {
      case 3: seed = atoi( argv[2] );                    // allow repeatable experiment
      case 2: times = atoi( argv[1] );                   // control recursion depth
      case 1: break;                                     // defaults
      default: cerr << "Usage: " << argv[0] << " times seed" << endl; exit( EXIT_FAILURE );
    }
    srand( seed );                                       // fixed or random seed
    
    H h(times); 					 // create the functor
    f(times, h);
}
