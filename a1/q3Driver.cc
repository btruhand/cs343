#include "q3coroutine.h"
#include <iostream>
#include <fstream>
using namespace std;

void uMain::main() {
    istream* input = &cin;
    switch(argc) {
	case 2:
	    try {
		input = new ifstream(argv[1]);
	    } catch (uFile::Failure) {
		cerr << "Error! Could not open file \"" << argv[1] << "\"" << endl;
	    }
	case 1:
	    break;
	default:
	    cerr << "Usage: " << argv[0] << " [ filename ]" << endl;
	    exit(EXIT_FAILURE);
    }
    
    char ch;
    *input >> noskipws;
    while(1) {
	int state; // state variable to be passed in to utf8checker
	unsigned int unicode; // variable to be passed in to utf8checker
	*input >> ch;
	if(input->fail()) break; // EOF
	if(ch == '\n') { 
	    // if it is actually an empty line
	    cout << " : Warning! Blank line." << endl;
	    continue; // go to the next iteration
	}
	
	cout << "0x" << hex << (unsigned int)(unsigned char)ch;
	Utf8 utf8checker(state, unicode);
	for(;;) {
	    if(state == WORKING) {
		utf8checker.next(ch);
		if(state != WORKING) {
		    if(state == MATCH) {
			// valid UTF8
			cout << " : valid 0x" << hex << unicode;
		    } else if(state == ERROR) {
			// invalid UTF8
			cout << " : invalid";
		    }
		    *input >> ch;
		    if(ch == '\n') break;
		    cout << ". Extra characters 0x" << hex << (unsigned int)(unsigned char)ch;
		}
	    }

	    *input >> ch;
	    if(ch == '\n') {
		if(state == WORKING) {
		    // this would indicate that the utf8 byte sequence is incomplete
		    // thus we call next to throw an Error
		    utf8checker.next(ch);
		    cout << " : invalid";
		}
		break;
	    }

	    if((unsigned int)(unsigned char)ch >= 0x0 && (unsigned int)(unsigned char)ch <= 0x9) {
		// check for 0-padding requirement
		cout << "0";
	    }
	    
	    cout << hex << (unsigned int)(unsigned char)ch;
	}
	cout << endl;
    }

    if(input != &cin) delete input;
}
